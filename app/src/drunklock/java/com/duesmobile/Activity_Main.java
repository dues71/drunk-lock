package com.duesmobile;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.ViewDragHelper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.duesmobile.R;
import com.duesmobile.models.Contact;
import com.duesmobile.ui.Fragment_Basic_Settings;
import com.duesmobile.ui.Fragment_Block_Settings;
import com.duesmobile.ui.Fragment_Main;
import com.duesmobile.ui.Fragment_Recent_Contacts;
import com.duesmobile.util.Util;

import java.lang.reflect.Field;
import java.util.ArrayList;

@SuppressLint({"Recycle", "UseValueOf"})
public class Activity_Main extends FragmentActivity {

    public static ArrayList<Contact> contactList = new ArrayList<Contact>();
    public static String titleDrunkLock = "Drunk Lock";
    public final static String titleBasicSettings = "Basic Settings";
    public final static String titleBlockingSettings = "Blocking Settings";
    public final static String titleRecentContacts = "Recently Added Contacts";
    public static final Fragment fragmentDrunkLock = new Fragment_Main();
    public static final Fragment fragmentBasicSettings = new Fragment_Basic_Settings();
    public static final Fragment fragmentBlockSettings = new Fragment_Block_Settings();
    public static final Fragment fragmentRecentContacts = new Fragment_Recent_Contacts();
    private final static String titleDivider = "-";
    public static final String[] navDrawerTitle = new String[]{titleDrunkLock, titleDivider,
            titleBasicSettings, titleBlockingSettings,
            titleRecentContacts, titleDivider};
    /*
     * Start Public Variable Declaration
     */
    static Context mContext;
    public boolean onResume = false;
    String TAG = "Activity_Main";
    private int mSelectedItem = 0;
    // Declare Variables
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private MenuListAdapter mMenuAdapter;
    private Runnable mPendingRunnable;
    private CharSequence mDrawerTitle;
    @SuppressWarnings("unused")
    private Fragment mPendingFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from drawer_main.xml
        setContentView(R.layout.activity_main);
        //  Util.currentActivity = this;
        //    mDrawerLayout.setDrawerShadow(getResources().getDrawable(R.color.color_transparent), GravityCompat.START);
        titleDrunkLock = getResources().getString(R.string.app_name);
        mContext = this;
        mDrawerList = (ListView) findViewById(R.id.list_view_drawer);
        mMenuAdapter = new MenuListAdapter(Activity_Main.this, navDrawerTitle);
        mDrawerList.setAdapter(mMenuAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
//        View footerView = View.inflate(mContext, R.layout.list_item_drawer_footer, null);
//        ImageView ivIcon = (ImageView) footerView.findViewById(R.id.icon);
//        TextView tvTitle = (TextView) footerView.findViewById(R.id.title);
//        tvTitle.setText(titleLogout);
//        ivIcon.setImageResource(R.drawable.ic_drawer_logout_grey);
//
//        footerView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // Logout Code
//                Util.CreateGAEvent(Activity_Main.this, getApplication(), SupportMethods.GA_MENU_ITEM_EVENT, "logout_menuItem");
//                SharedPreferencesManager.clearUserSession(mContext);
//                CarrierDB db = new CarrierDB(mContext);
//                db.open();
//                db.deleteAllFavorites();
//                db.close();
//                ((CarrierApplication) getApplication()).mTracker = null;
//                Intent loginIntent = new Intent(Activity_Main.this, Activity_Login.class);
//                startActivity(loginIntent);
//                finish();
//            }
//        });
      //  mDrawerList.addFooterView(footerView);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerShadow(R.drawable.ab_transparent_drunklock, GravityCompat.START);
            getActionBar().setHomeButtonEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_navigation_drawer, R.string.drawer_open, R.string.drawer_close) {

                public void onDrawerClosed(View view) {
                    setTitle(navDrawerTitle[mSelectedItem]);
                    supportInvalidateOptionsMenu();
                    super.onDrawerClosed(view);
                }

                public void onDrawerOpened(View drawerView) {
                    setTitle("Carrier Dashboard Menu");
                    try {
                        supportInvalidateOptionsMenu();
                  //      InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                      //  imm1.hideSoftInputFromWindow(currentEditText.getWindowToken(), 0);
                    } catch (Exception ignored) {
                    }
                    getActionBar().setDisplayShowHomeEnabled(true);
                    Util.CreateGAScreenView(mContext, "HomeScreen Menu");
                    super.onDrawerOpened(drawerView);
                }
            };

            mDrawerLayout.setDrawerListener(mDrawerToggle);
            // Increase the Drag Margin of the navigation drawer
            Field mDragger;
            try {
                mDragger = mDrawerLayout.getClass().getDeclaredField("mLeftDragger");
                mDragger.setAccessible(true);
                ViewDragHelper draggerObj = (ViewDragHelper) mDragger.get(mDrawerLayout);
                Field mEdgeSize = draggerObj.getClass().getDeclaredField("mEdgeSize");
                mEdgeSize.setAccessible(true);
                int edge = mEdgeSize.getInt(draggerObj);
                mEdgeSize.setInt(draggerObj, edge * 2);
            } catch (Exception ignored) {
            }
            if (savedInstanceState == null) {
                selectItem(0);
            }
        } else {
            getActionBar().setHomeButtonEnabled(false);
            getActionBar().setDisplayHomeAsUpEnabled(false);
        }

        // Initialize the Freight Finder Screen
        // Initialize the Find Freight Screen
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragmentDrunkLock);
        ft.commit();
        getActionBar().setTitle(getResources().getString(R.string.app_name));
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean(Util.PREFS_NAV_START, true)) {
            SharedPreferences.Editor prefEdit = prefs.edit();
            prefEdit.putBoolean(Util.PREFS_NAV_START, false);
            prefEdit.commit();
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
//        // Create new transaction
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.content_frame, findFreightFragment);
//        ft.commit();
//        // Change Title
//        getSupportActionBar().setTitle("Freight Finder");
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        if (prefs.getBoolean(SharedPreferencesManager.PREFS_NAV_START, true)) {
//            SharedPreferences.Editor prefEdit = prefs.edit();
//            prefEdit.putBoolean(SharedPreferencesManager.PREFS_NAV_START, false);
//            prefEdit.commit();
//            mDrawerLayout.openDrawer(Gravity.LEFT);
//        }
//        FragmentTransaction fragmentTrans = getSupportFragmentManager().beginTransaction();
//        if (mPendingFragment == null) {
//            fragmentTrans.replace(R.id.content_frame, findFreightFragment);
//            mPendingFragment = findFreightFragment;
//            currentFragmentTitle = titleFreightFinder;
//            fragmentTrans.commit();
//        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerLayout != null) {
            if (item.getItemId() == android.R.id.home) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (mPendingFragment != null) {
            mPendingFragment.onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mDrawerLayout != null) {
            boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            for (int i = 0; i < menu.size(); i++) {
                menu.getItem(i).setVisible(!drawerOpen);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void selectItem(int position) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (navDrawerTitle[position].equals(titleDrunkLock)) {
            mSelectedItem = position;
            mMenuAdapter.notifyDataSetChanged();
            Util.CreateGAEvent(Activity_Main.this, "menuItem_click", "Home_Screen");
            ft.replace(R.id.content_frame, fragmentDrunkLock).commit();
            mPendingFragment = fragmentDrunkLock;
        } else if (navDrawerTitle[position].equals(titleBasicSettings)) {
            mSelectedItem = position;
            mMenuAdapter.notifyDataSetChanged();
            Util.CreateGAEvent(Activity_Main.this, "menuItem_click", "Basic_Settings");
            ft.replace(R.id.content_frame, fragmentBasicSettings).commit();
            mPendingFragment = fragmentBasicSettings;
        } else if (navDrawerTitle[position].equals(titleBlockingSettings)) {
            mSelectedItem = position;
            mMenuAdapter.notifyDataSetChanged();
            Util.CreateGAEvent(Activity_Main.this, "menuItem_click", "Block_Settings");
            ft.replace(R.id.content_frame, fragmentBlockSettings).commit();
            mPendingFragment = fragmentBlockSettings;
        } else if (navDrawerTitle[position].equals(titleRecentContacts)) {
            mSelectedItem = position;
            mMenuAdapter.notifyDataSetChanged();
            Util.CreateGAEvent(Activity_Main.this, "menuItem_click", "Recent_Contacts");
            ft.replace(R.id.content_frame, fragmentRecentContacts).commit();
            mPendingFragment = fragmentRecentContacts;
        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mDrawerTitle = title;
        getActionBar().setTitle(mDrawerTitle);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }

            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            FragmentManager manager = getFragmentManager();
            // // AUTO Log.e(TAG, " manager.getBackStackEntryCount() = " +
            // manager.getBackStackEntryCount());
            if (manager.getBackStackEntryCount() == 0) {
                finish();
            } else {
                manager.popBackStack();
            }
            return true;
        }
        return false;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    public class MenuListAdapter extends BaseAdapter {

        public final int[] mIconGrey = new int[]{R.drawable.ic_action_share, 0,
                R.drawable.ic_action_share, R.drawable.ic_action_share,
                R.drawable.ic_action_share, 0  };
        public final int[] mIconBlue = new int[]{R.drawable.ic_action_add, 0,
                R.drawable.ic_action_add, R.drawable.ic_action_add,
                R.drawable.ic_action_add, 0 };        // Declare Variables
        private final Context mContext;

        public MenuListAdapter(Context context, String[] title) {
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return navDrawerTitle.length;
        }

        @Override
        public Object getItem(int position) {
            return navDrawerTitle[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            @SuppressLint("ViewHolder") final View view = View.inflate(mContext, R.layout.list_item_drawer, null);
            ImageView ivIcon = (ImageView) view.findViewById(R.id.icon);
            TextView tvTitle = (TextView) view.findViewById(R.id.title);
            LinearLayout llDrawerItem = (LinearLayout) view.findViewById(R.id.ll_drawer_item);
            LinearLayout llDivider = (LinearLayout) view.findViewById(R.id.ll_drawer_item_divider);
            if (navDrawerTitle[position].equals(titleDivider)) {
                llDrawerItem.setVisibility(View.GONE);
                llDivider.setVisibility(View.VISIBLE);
            } else {
                llDrawerItem.setVisibility(View.VISIBLE);
                llDivider.setVisibility(View.GONE);
                if (position == mSelectedItem) {
                    tvTitle.setTextColor(mContext.getResources().getColor(R.color.color_app_default));
                    ivIcon.setImageResource(mIconBlue[position]);
                    //   llListItem.setBackgroundResource(R.drawable.btn_drawer_selected);
                } else {
                    tvTitle.setTextColor(mContext.getResources().getColor(R.color.color_white));
                    ivIcon.setImageResource(mIconGrey[position]);
                    //    llListItem.setBackgroundResource(R.drawable.btn_drawer);
                }

                // Set the results into the views
                tvTitle.setText(navDrawerTitle[position]);
            }


            //  tvSubTitle.setText(mSubTitle[position]);
            //   tvSubTitle.setTypeface(gothamLight);
            //    ivIcon.setImageResource(mIcon[position]);
            return view;
        }
    }
}