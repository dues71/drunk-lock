package com.duesmobile.models;

public class AlarmItem {

    public String alarmID;
    public Integer alarmStartHour;
    public Integer alarmStartMinute;
    public String alarmLength;
    public boolean alarmRepeating;
    public String alarmDaysRepeating;
    public boolean alarmAlwaysOn;
    public boolean alarmStatus;

    public AlarmItem() {

    }

//    public AlarmItem(String alarmID,Integer alarmStartHour, Integer alarmStartMinute, String alarmLength, boolean alarmRepeating, String alarmDaysRepeating, boolean alarmAlwaysOn, boolean alarmStatus) {
//
//        this.alarmID = alarmID;
//        this.alarmStartHour = alarmStartHour;
//        this.alarmStartMinute = alarmStartMinute;
//        this.alarmLength = alarmLength;
//        this.alarmRepeating = alarmRepeating;
//        this.alarmDaysRepeating = alarmDaysRepeating;
//        this.alarmAlwaysOn = alarmAlwaysOn;
//        this.alarmStatus = alarmStatus;
//
//    }
}
