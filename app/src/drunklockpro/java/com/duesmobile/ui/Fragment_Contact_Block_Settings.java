package com.duesmobile.ui;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.duesmobile.R;
import com.duesmobile.util.DrunkLockDB;
import com.duesmobile.util.RepeatSafeToast;
import com.duesmobile.util.Util;

import java.util.ArrayList;
import java.util.List;

import static android.provider.ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
import static android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

public class Fragment_Contact_Block_Settings extends ListFragment {

    private final int PICK_CONTACT = 1;
    private LinearLayout llSpecificContacts;
    private RadioButton rgBlockAll;
    private RadioButton rgBlockSpecific;
    private EditText contactName;
    private EditText contactNumber;
    // Capture third menu button click
    private String c_id;
    private String c_name;
    private String c_number;
    private Context cxt;
    private Activity activity;
    private ContactsListAdapter listAdapter;
    private ArrayList<String[]> blockedContactsList = new ArrayList<String[]>();
    private View contactBlockView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contactBlockView = inflater.inflate(R.layout.fragment_contact_block_settings, container, false);
        setHasOptionsMenu(true);
        cxt = getActivity();
        activity = getActivity();
        Util.CreateGAScreenView(cxt, "Contact Block Settings");
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final SharedPreferences.Editor prefEdit = prefs.edit();

        Button selectContact = (Button) contactBlockView.findViewById(R.id.btn_selectContactBlock);
        Button addContactBtn = (Button) contactBlockView.findViewById(R.id.btn_addContactBlock);
        contactName = (EditText) contactBlockView.findViewById(R.id.et_contactName);
        contactNumber = (EditText) contactBlockView.findViewById(R.id.et_contactNumber);
        Button addNewContact = (Button) contactBlockView.findViewById(R.id.btn_addNewContact);
        RadioGroup rgContactBlock = (RadioGroup) contactBlockView.findViewById(R.id.rg_contact_block);
        rgBlockAll = (RadioButton) contactBlockView.findViewById(R.id.radio_block_all);
        rgBlockSpecific = (RadioButton) contactBlockView.findViewById(R.id.radio_block_specific);
        llSpecificContacts = (LinearLayout) contactBlockView.findViewById(R.id.ll_block_specific_contact);

        if (prefs.getBoolean(Util.KEY_CONTACT_BLOCK_ALL, true)) {
            llSpecificContacts.setVisibility(View.GONE);
            rgBlockAll.setChecked(true);
        } else {
            llSpecificContacts.setVisibility(View.VISIBLE);
            rgBlockSpecific.setChecked(true);
        }
        rgContactBlock.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (!Util.isMyServiceRunning(getActivity())) {
                    if (checkedId == rgBlockAll.getId()) {
                        prefEdit.putBoolean(Util.KEY_CONTACT_BLOCK_ALL, true);
                        llSpecificContacts.setVisibility(View.GONE);
                    } else if (checkedId == rgBlockSpecific.getId()) {
                        llSpecificContacts.setVisibility(View.VISIBLE);
                        prefEdit.putBoolean(Util.KEY_CONTACT_BLOCK_ALL, false);
                    }
                    prefEdit.commit();
                } else {
                    if (prefs.getBoolean(Util.KEY_CONTACT_BLOCK_ALL, true)) {
                        llSpecificContacts.setVisibility(View.GONE);
                        rgBlockAll.setChecked(true);
                    } else {
                        llSpecificContacts.setVisibility(View.VISIBLE);
                        rgBlockSpecific.setChecked(true);
                    }
                    RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
                }
            }
        });
        selectContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Declare
                if (!Util.isMyServiceRunning(getActivity())) {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, PICK_CONTACT);
                } else {
                    RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
                }
            }
        });
        addContactBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!Util.isMyServiceRunning(getActivity())) {
                    if (contactBlockView.findViewById(R.id.layout_addContact).getVisibility() == View.GONE) {
                        contactBlockView.findViewById(R.id.layout_addContact).setVisibility(View.VISIBLE);

                    } else {

                        contactBlockView.findViewById(R.id.layout_addContact).setVisibility(View.GONE);
                    }
                } else {
                    RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
                }
            }
        });
        addNewContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!Util.isMyServiceRunning(getActivity())) {
                    if (contactBlockView.findViewById(R.id.layout_addContact).getVisibility() == View.GONE) {
                        contactBlockView.findViewById(R.id.layout_addContact).setVisibility(View.VISIBLE);

                    } else {
                        boolean isBlocked = false;
                        c_number = contactNumber.getText().toString();
                        c_name = contactName.getText().toString();
                        c_id = "";
                        for (String[] currentContacts2 : blockedContactsList) {

                            if (currentContacts2[1].equals(c_number)) {

                                isBlocked = true;
                            }
                        }
                        if (!isBlocked) {

                            DrunkLockDB dbAdd = new DrunkLockDB(cxt);
                            dbAdd.open();
                            dbAdd.insertContact(c_name, c_number);
                            dbAdd.close();
                            String[] newContact = {c_id, c_number, c_name};
                            blockedContactsList.add(newContact);
                        } else {
                            Toast.makeText(cxt, "This contact has already been added to your list of blocked contacts.", Toast.LENGTH_LONG).show();
                        }
                        contactBlockView.findViewById(R.id.layout_addContact).setVisibility(View.GONE);
                    }
                    c_number = null;
                    populateContactsList();
                } else {
                    RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
                }
            }

        });

        return contactBlockView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        populateContactsList();
    }

    // code
    @SuppressWarnings("deprecation")
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c = activity.managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {

                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        c_id = id;
                        // Log.e("test", "id is:" + id);
                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone != null) {
                            if (hasPhone.equalsIgnoreCase("1")) {
                                Cursor phones = null;
                                if (CONTENT_URI != null) {
                                    phones = activity.getContentResolver().query(CONTENT_URI, null, CONTACT_ID + " = " + id,
                                            null, null);
                                }
                                if (phones != null) {
                                    phones.moveToFirst();

                                    c_number = phones.getString(phones.getColumnIndex("data1"));
                                }
                                // Log.e("tester", "number is: " + cNumber);
                            }
                        }
                        c_name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        // Log.e("tester", "number is: " + c_number);
                        if (c_number == null) {
                            Toast.makeText(cxt, "The contact you selected does not have a phone number associated with it.", Toast.LENGTH_LONG).show();
                        } else {
                            boolean isBlocked = false;
                            for (String[] currentContacts2 : blockedContactsList) {

                                if (currentContacts2[1].equals(c_number)) {

                                    isBlocked = true;
                                }
                            }
                            if (!isBlocked) {
                                DrunkLockDB dbAdd = new DrunkLockDB(cxt);
                                dbAdd.open();
                                dbAdd.insertContact(c_name, c_number);
                                dbAdd.close();
                                String[] newContact = {c_id, c_number, c_name};
                                blockedContactsList.add(newContact);
                            } else {
                                Toast.makeText(cxt, "This contact has already been added to your list of blocked contacts.", Toast.LENGTH_LONG).show();
                            }

                        }
                        c_number = null;
                        populateContactsList();
                    }
                }
                break;
        }
    }

    void populateContactsList() {
        blockedContactsList = new ArrayList<String[]>();
        DrunkLockDB db = new DrunkLockDB(getActivity());
        db.open();
        Cursor c = db.getAllContacts();
        while (c.moveToNext()) {
            String[] application = {c.getString(0), c.getString(1), c.getString(2)};
            blockedContactsList.add(application);
        }
        db.close();
        listAdapter = new ContactsListAdapter(getActivity(), R.layout.list_item_contact, blockedContactsList);
        setListAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
//        getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
//
//            @SuppressWarnings("rawtypes")
//            @Override
//            public boolean onItemLongClick(AdapterView parent, View arg1, int position, long arg3) {
//                String[] contact = blockedContactsList.get(position);
//                DrunkLockProDB dbRemove = new DrunkLockProDB(getActivity());
//                dbRemove.open();
//                dbRemove.deleteContact(Long.parseLong(contact[0]));
//                dbRemove.close();
//                blockedContactsList.remove(position);
//                listAdapter.notifyDataSetChanged();
//                return false;
//
//            }
//        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Util.isMyServiceRunning(cxt)) {
            return;
        }
        populateContactsList();
    }

    public class ContactsListAdapter extends ArrayAdapter<String[]> {

        public ContactsListAdapter(Context context, int textViewResourceId, List<String[]> blockedApplicationList) {
            super(context, textViewResourceId, blockedApplicationList);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_contact, parent, false);
            // Set Font
            String[] contact = blockedContactsList.get(position);
            // Define Views
            if (convertView != null) {
                TextView tvLabel = (TextView) convertView.findViewById(R.id.tv_contact_name);
                TextView tvPackageName = (TextView) convertView.findViewById(R.id.tv_contact_number);
                ImageView ivContactRemove = (ImageView) convertView.findViewById(R.id.iv_contact_remove);
                ivContactRemove.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            String[] contact = blockedContactsList.get(position);
                            DrunkLockDB dbRemove = new DrunkLockDB(getActivity());
                            dbRemove.open();
                            dbRemove.deleteContact(Long.parseLong(contact[0]));
                            dbRemove.close();
                            blockedContactsList.remove(position);
                            listAdapter.notifyDataSetChanged();
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }
                    }
                });
                tvLabel.setText(contact[1]);
                tvPackageName.setText(contact[2]);
            }
            return convertView;
        }
    }

}
