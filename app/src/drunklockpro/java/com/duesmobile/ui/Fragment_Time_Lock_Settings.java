package com.duesmobile.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.duesmobile.R;
import com.duesmobile.models.AlarmItem;
import com.duesmobile.services.Service_Starter;
import com.duesmobile.services.Service_Terminator;
import com.duesmobile.util.DrunkLockDB;
import com.duesmobile.util.RepeatSafeToast;
import com.duesmobile.util.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Fragment_Time_Lock_Settings extends ListFragment {

    // Capture third menu button click
    private final MenuItem.OnMenuItemClickListener AddApplicationClickListener = new MenuItem.OnMenuItemClickListener() {

        public boolean onMenuItemClick(MenuItem item) {
            if (Util.isMyServiceRunning(getActivity())) {
                RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
            } else {
                TimeAlert(false, 0);
            }
            return false;
        }
    };
    private Integer startHour;
    private Integer startMinute;
    //    private boolean alwaysOn = false;
    private double duration = 4.0;
    private boolean setRepeating = false;
    private String repeatingDays;
    private Context cxt;
    private AlarmListAdapter listAdapter;
    private ArrayList<AlarmItem> alarmList = new ArrayList<AlarmItem>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View timeLockView = inflater.inflate(R.layout.fragment_time_lock_settings, container, false);
        setHasOptionsMenu(true);
        cxt = getActivity();
        Util.CreateGAScreenView(cxt, "Time Lock Settings");
        return timeLockView;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add("Schedule Drunk Lock").setIcon(R.drawable.ic_action_add).setOnMenuItemClickListener(AddApplicationClickListener).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

    }

    private void TimeAlert(final boolean edit, final int listID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(cxt);
        final TimePicker tp = new TimePicker(cxt);
        //  tp.setBackgroundColor(getResources().getColor(R.color.color_beer_white));
        tp.setIs24HourView(false);
        try {
            tp.setCurrentHour(startHour);
            tp.setCurrentMinute(startMinute);
        } catch (Exception ignored) {
        }
        builder.setView(tp);
        builder.setTitle("Select Start Time");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                if (edit) {
                    alarmList.get(listID).alarmStartHour = tp.getCurrentHour();
                    alarmList.get(listID).alarmStartMinute = tp.getCurrentMinute();
                    listAdapter.notifyDataSetChanged();
                    alarmList.get(listID).alarmStatus = false;
                    UpdateAlarm(Long.valueOf(alarmList.get(listID).alarmID), alarmList.get(listID).alarmStartHour, alarmList.get(listID).alarmStartMinute, alarmList.get(listID).alarmLength, alarmList.get(listID).alarmRepeating,
                            alarmList.get(listID).alarmDaysRepeating, alarmList.get(listID).alarmAlwaysOn, alarmList.get(listID).alarmStatus);
                } else {
                    tp.setIs24HourView(true);
                    startHour = tp.getCurrentHour();
                    startMinute = tp.getCurrentMinute();

                    tp.setIs24HourView(false);
                    //  startTime = new StringBuilder().append(startHour).append(":").append(startMinute).toString();
                    OptionsAlert();
                }

                dialog.cancel();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {

                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();
        // builder.setInverseBackgroundForced(true);
        alert.show();

    }

    private void OptionsAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(cxt);

        LinearLayout llMain = new LinearLayout(cxt);
        LinearLayout llSeek = new LinearLayout(cxt);
        final SeekBar sbDuration = new SeekBar(cxt);
        final TextView tvSeekBar = new TextView(cxt);
        TextView tvInfo = new TextView(cxt);
        LinearLayout.LayoutParams lpSeek = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        // lpMain.set
        tvInfo.setText("Set Duration:");
        // LayoutParams lpInfo = new
        // LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
        // LayoutParams.WRAP_CONTENT);
        tvInfo.setPadding(20, 5, 20, 5);
        tvInfo.setGravity(Gravity.CENTER_VERTICAL);
        // tvInfo.setLayoutParams(lpInfo);
        llSeek.setLayoutParams(lpSeek);
        llMain.setOrientation(LinearLayout.VERTICAL);
        llSeek.setGravity(Gravity.CENTER_VERTICAL);
        llSeek.setOrientation(LinearLayout.HORIZONTAL);

        sbDuration.setMax(48);
        sbDuration.setProgress(8);
        sbDuration.incrementProgressBy(1);
        // sbDuration.setMinimumWidth(200);
        LayoutParams lp1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1.0f);
        sbDuration.setLayoutParams(lp1);
        LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 0.25f);
        tvSeekBar.setLayoutParams(lp2);
        tvSeekBar.setText("4.0 hours");
        sbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvSeekBar.setText(String.valueOf((double) progress / 2) + " hours");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
//        final CheckBox unlock = new CheckBox(cxt);
//        unlock.setText("Don't let me unlock during this time");
        llSeek.addView(sbDuration);
        llSeek.addView(tvSeekBar);
        llMain.addView(tvInfo);
        llMain.addView(llSeek);
//        llMain.addView(unlock);
        builder.setView(llMain);
        builder.setTitle("Schedule Options");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
//                alwaysOn = unlock.isChecked();
                duration = (double) sbDuration.getProgress() / 2;
                RepeatingAlert();
                dialog.cancel();
            }
        }).setNegativeButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                TimeAlert(false, 0);
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    void RepeatingAlert() {
        cxt.getSystemService(Context.INPUT_METHOD_SERVICE);
        AlertDialog.Builder alert = new AlertDialog.Builder(cxt);
        alert.setTitle("Select Repeating Days");
        final LinearLayout dialogLayout = new LinearLayout(cxt);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        final CheckBox cbRepeat = new CheckBox(cxt);
        cbRepeat.setText("Set Repeating");
        ArrayList<String> adapterList = new ArrayList<String>();
        adapterList.add("Sunday");
        adapterList.add("Monday");
        adapterList.add("Tuesday");
        adapterList.add("Wednesday");
        adapterList.add("Thursday");
        adapterList.add("Friday");
        adapterList.add("Saturday");
        DayAdapter arrayAdapter =
                //= new ArrayAdapter<String>(getActivity(), R.layout.multichoice_custom, adapterList,new Int[] {R.id.textView1})
                //   final ArrayAdapter arrayAdapter =new ArrayAdapter<String>(getActivity(), R.layout.multichoice_custom, adapterList,        final ArrayAdapter arrayAdapter =new ArrayAdapter<String>(getActivity(), R.layout.multichoice_custom, adapterList,R.id.textView1);
                //    new ArrayAdapter<String>(getActivity(), R.layout.multichoice_custom, adapterList,new int[] {R.id.textView1});

                new DayAdapter(cxt, R.layout.multichoice_custom, adapterList);
        final ListView lv = new ListView(cxt);
        dialogLayout.addView(cbRepeat);
        lv.setAdapter(arrayAdapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        dialogLayout.addView(lv);
        lv.setVisibility(View.GONE);
        cbRepeat.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    lv.setVisibility(View.VISIBLE);
                } else {
                    lv.setVisibility(View.GONE);
                }
            }

        });
        alert.setView(dialogLayout);
        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (cbRepeat.isChecked()) {
                    SparseBooleanArray sparseBooleanArray = lv.getCheckedItemPositions();
                    setRepeating = true;
                    repeatingDays = "";
                    for (int i = 0; i < lv.getCount(); i++) {

                        if (sparseBooleanArray != null) {
                            if (sparseBooleanArray.get(i)) {
                                repeatingDays = repeatingDays + "|" + lv.getItemAtPosition(i).toString();
                            }
                        }

                    }
                } else {
                    repeatingDays = "";
                    setRepeating = false;
                }
                SaveAlarm();
                dialog.dismiss();
            }
        });

        alert.setNegativeButton("Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                OptionsAlert();
                dialog.dismiss();
            }
        });
        alert.show();
    }

    void DeleteAlarm(AlarmItem alarm, int position) {
        DrunkLockDB dbRemove = new DrunkLockDB(getActivity());
        DestroyAlarms(alarm);
        dbRemove.open();
        dbRemove.deleteAlarm(Long.valueOf(alarm.alarmID));
        dbRemove.close();
        alarmList.remove(position);
        listAdapter.notifyDataSetChanged();
    }

    void SaveAlarm() {
        DrunkLockDB dbAdd = new DrunkLockDB(getActivity());
        AlarmItem alarm = new AlarmItem();
        alarm.alarmStartHour = startHour;
        alarm.alarmStartMinute = startMinute;
        alarm.alarmLength = String.valueOf(duration);
        alarm.alarmRepeating = setRepeating;
        alarm.alarmDaysRepeating = repeatingDays;
        alarm.alarmAlwaysOn = false;
//        alarm.alarmAlwaysOn = alwaysOn;
        alarm.alarmStatus = true;
        dbAdd.open();
        alarm.alarmID = String.valueOf(dbAdd.insertAlarm(alarm.alarmStartHour, alarm.alarmStartMinute, alarm.alarmLength, alarm.alarmRepeating,
                alarm.alarmDaysRepeating, alarm.alarmAlwaysOn, alarm.alarmStatus));
        dbAdd.close();

        SetAlarms(alarm);
        PopulateApplicationsList();
    }

    void UpdateAlarm(Long id, Integer startHour, Integer startMinute, String length, boolean repeating, String days, boolean alwaysOn, boolean status) {
        DrunkLockDB dbUpdate = new DrunkLockDB(getActivity());
        dbUpdate.open();
        dbUpdate.updateAlarm(id, startHour, startMinute, length, repeating, days, alwaysOn, status);
        dbUpdate.close();
        //	PopulateApplicationsList();
    }

    void DestroyAlarms(AlarmItem alarm) {
        Intent startIntent = new Intent(getActivity(), Service_Starter.class);
        Intent endIntent = new Intent(getActivity(), Service_Terminator.class);
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        if (alarm.alarmRepeating) {
            Log.e("temp", "Repeating Alarm Destroy");
            List<Integer> dayArray = new ArrayList<Integer>();
            if (alarm.alarmDaysRepeating.contains("Sun")) {
                dayArray.add(Calendar.SUNDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Mon")) {
                dayArray.add(Calendar.MONDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Tue")) {
                dayArray.add(Calendar.TUESDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Wed")) {
                dayArray.add(Calendar.WEDNESDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Thu")) {
                dayArray.add(Calendar.THURSDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Fri")) {
                dayArray.add(Calendar.FRIDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Sat")) {
                dayArray.add(Calendar.SATURDAY);
            }

            for (int i = 1; i <= dayArray.size(); ++i) {
                // Destroy Single Start Alarm
                PendingIntent pendingStartIntent = PendingIntent.getService(getActivity(), Integer.valueOf("1" + String.valueOf(i) + alarm.alarmID), startIntent, 0);
                alarmManager.cancel(pendingStartIntent);

                // Destroy Single End Alarm
                PendingIntent pendingEndIntent = PendingIntent.getService(getActivity(), Integer.valueOf("2" + String.valueOf(i) + alarm.alarmID), endIntent, 0);
                alarmManager.cancel(pendingEndIntent);

            }
        } else {
            Log.e("temp", "Single Alarm Destroy");
            // Destroy Single Start Alarm
            PendingIntent pendingStartIntent = PendingIntent.getService(getActivity(), Integer.valueOf("1" + alarm.alarmID), startIntent, 0);
            alarmManager.cancel(pendingStartIntent);

            // Destroy Single End Alarm
            PendingIntent pendingEndIntent = PendingIntent.getService(getActivity(), Integer.valueOf("2" + alarm.alarmID), endIntent, 0);
            alarmManager.cancel(pendingEndIntent);
        }
    }

    void SetAlarms(AlarmItem alarm) {
        // This function sets 14 alarms based upon the alarm id
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent startIntent = new Intent(getActivity(), Service_Starter.class);
        startIntent.putExtra("duration", alarm.alarmLength);
        Intent endIntent = new Intent(getActivity(), Service_Terminator.class);
        Log.e("Start", "Start");
        int aMin;
        int aHour;
        if (alarm.alarmLength.contains(".5")) {
            aMin = 30;
            aHour = Integer.valueOf(alarm.alarmLength.substring(0, alarm.alarmLength.indexOf(".")));
        } else if (alarm.alarmLength.contains(".")) {
            aMin = 0;
            aHour = Integer.valueOf(alarm.alarmLength.substring(0, alarm.alarmLength.indexOf(".")));
        } else {
            aMin = 0;
            aHour = Integer.valueOf(alarm.alarmLength);
        }

        if (alarm.alarmRepeating) {
            Log.e("temp", "Repeating Alarm Set");
            List<Integer> dayArray = new ArrayList<Integer>();
            if (alarm.alarmDaysRepeating.contains("Sun")) {
                dayArray.add(Calendar.SUNDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Mon")) {
                dayArray.add(Calendar.MONDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Tue")) {
                dayArray.add(Calendar.TUESDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Wed")) {
                dayArray.add(Calendar.WEDNESDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Thu")) {
                dayArray.add(Calendar.THURSDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Fri")) {
                dayArray.add(Calendar.FRIDAY);
            }
            if (alarm.alarmDaysRepeating.contains("Sat")) {
                dayArray.add(Calendar.SATURDAY);
            }

            for (int i = 1; i <= dayArray.size(); ++i) {

                int sHour = alarm.alarmStartHour;
                int sMin = alarm.alarmStartMinute;
                int eDay = i;
                int eHour = sHour + aHour;
                int eMin = sMin + aMin;
                if (eMin >= 60) {
                    eHour = eHour + 1;
                    eMin = eMin - 60;
                }
                if (eHour >= 24) {
                    eHour = eHour - 24;
                    if (eDay + 1 > 7) {
                        eDay = 0;
                    } else {
                        eDay = eDay + 1;
                    }
                }
                // Create Single Start Alarm
                PendingIntent pendingStartIntent = PendingIntent.getService(getActivity(), Integer.valueOf("1" + String.valueOf(i) + alarm.alarmID), startIntent, 0);
                Calendar startCalendar = Calendar.getInstance();
                startCalendar.set(Calendar.DAY_OF_WEEK, i);
                startCalendar.set(Calendar.HOUR_OF_DAY, sHour);
                startCalendar.set(Calendar.MINUTE, sMin);
                startCalendar.set(Calendar.SECOND, 0);
                alarmManager.setRepeating(AlarmManager.RTC,
                        startCalendar.getTimeInMillis(), 24 * 60 * 60 * 100, pendingStartIntent);

                // Create Single End Alarm
                PendingIntent pendingEndIntent = PendingIntent.getService(getActivity(), Integer.valueOf("2" + String.valueOf(i) + alarm.alarmID), endIntent, 0);
                Calendar endCalendar = Calendar.getInstance();
                endCalendar.set(Calendar.DAY_OF_WEEK, eDay);
                endCalendar.set(Calendar.HOUR_OF_DAY, eHour);
                endCalendar.set(Calendar.MINUTE, eMin);
                endCalendar.set(Calendar.SECOND, 0);
                alarmManager.setRepeating(AlarmManager.RTC,
                        endCalendar.getTimeInMillis(), 24 * 60 * 60 * 100, pendingEndIntent);
            }
        } else {
            Log.e("temp", "Single Alarm Set");
            Time curTime = new Time(Time.getCurrentTimezone());
            curTime.setToNow();
            boolean nextDay = false;

            if (alarm.alarmStartHour < curTime.hour) {
                nextDay = true;
            } else if (alarm.alarmStartHour == curTime.hour) {
                if (alarm.alarmStartMinute <= curTime.minute) {
                    nextDay = true;
                }
            }
            int sDay = curTime.weekDay + 1;
            int sHour = alarm.alarmStartHour;
            int sMin = alarm.alarmStartMinute;
            int eDay = curTime.weekDay + 1;
            if (nextDay) {
                if (sDay + 1 > 7) {
                    sDay = 0;
                } else {
                    sDay = sDay + 1;
                }
                if (eDay + 1 > 7) {
                    eDay = 0;
                } else {
                    eDay = eDay + 1;
                }
            }
            int eHour = sHour + aHour;
            int eMin = sMin + aMin;
            if (eMin >= 60) {
                eHour = eHour + 1;
                eMin = eMin - 60;
            }
            if (eHour >= 24) {
                eHour = eHour - 24;
                if (eDay + 1 > 7) {
                    eDay = 0;
                } else {
                    eDay = eDay + 1;
                }
            }
            // Create Single Start Alarm
            PendingIntent pendingStartIntent = PendingIntent.getService(getActivity(), Integer.valueOf("1" + alarm.alarmID), startIntent, 0);
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.set(Calendar.DAY_OF_WEEK, sDay);
            startCalendar.set(Calendar.HOUR_OF_DAY, sHour);
            startCalendar.set(Calendar.MINUTE, sMin);
            startCalendar.set(Calendar.SECOND, 0);
            alarmManager.set(AlarmManager.RTC,
                    startCalendar.getTimeInMillis(), pendingStartIntent);

            // Create Single End Alarm
            PendingIntent pendingEndIntent = PendingIntent.getService(getActivity(), Integer.valueOf("2" + alarm.alarmID), endIntent, 0);
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set(Calendar.DAY_OF_WEEK, eDay);
            endCalendar.set(Calendar.HOUR_OF_DAY, eHour);
            endCalendar.set(Calendar.MINUTE, eMin);
            endCalendar.set(Calendar.SECOND, 0);
            alarmManager.set(AlarmManager.RTC,
                    endCalendar.getTimeInMillis(), pendingEndIntent);

        }
    }

    void PopulateApplicationsList() {
        alarmList = new ArrayList<AlarmItem>();
        DrunkLockDB dbPopulate = new DrunkLockDB(getActivity());
        dbPopulate.open();
        Cursor c = dbPopulate.getAllAlarms();
        while (c.moveToNext()) {
            AlarmItem alarm = new AlarmItem();
            alarm.alarmID = c.getString(0);
            alarm.alarmStartHour = c.getInt(1);
            alarm.alarmStartMinute = c.getInt(2);
            alarm.alarmLength = c.getString(3);
            alarm.alarmRepeating = c.getInt(4) > 0;
            alarm.alarmDaysRepeating = c.getString(5);
            alarm.alarmAlwaysOn = c.getInt(6) > 0;
            alarm.alarmStatus = c.getInt(7) > 0;
            alarmList.add(alarm);
        }
        dbPopulate.close();
        listAdapter = new AlarmListAdapter(getActivity(), R.layout.list_item_alarm, alarmList);
        setListAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

            @SuppressWarnings("rawtypes")
            @Override
            public boolean onItemLongClick(AdapterView parent, View arg1, int position, long arg3) {
                AlarmItem alarm = alarmList.get(position);
                DrunkLockDB dbRemove = new DrunkLockDB(getActivity());
                dbRemove.open();
                dbRemove.deleteAlarm(Long.parseLong(alarm.alarmID));
                dbRemove.close();
                alarmList.remove(position);
                listAdapter.notifyDataSetChanged();
                return false;

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        PopulateApplicationsList();
    }

    public class DayAdapter extends ArrayAdapter<String> {

        final Context context;
        final int layoutResourceId;
        ArrayList<String> days = null;

        public DayAdapter(Context context, int layoutResourceId, ArrayList<String> data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.days = data;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);

            if (convertView != null) {
                CheckedTextView tvMultiChoice = (CheckedTextView) convertView.findViewById(R.id.ctv_multi_choice);
                tvMultiChoice.setText(days.get(position));
            }


            return convertView;
        }

    }

    public class AlarmListAdapter extends ArrayAdapter<AlarmItem> {

        public AlarmListAdapter(Context context, int textViewResourceId, List<AlarmItem> alarmList) {
            super(context, textViewResourceId, alarmList);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_alarm, parent, false);
            final AlarmItem alarm = alarmList.get(position);

            // Define Views
            if (convertView != null) {
                final TextView tvStartTime = (TextView) convertView.findViewById(R.id.tv_start_time);
                final TextView tvDuration = (TextView) convertView.findViewById(R.id.tv_duration);
                final CheckBox cbSetRepeating = (CheckBox) convertView.findViewById(R.id.cb_repeating);
                final SeekBar sbDuration = (SeekBar) convertView.findViewById(R.id.sb_duration);
                final ToggleButton tbStatus = (ToggleButton) convertView.findViewById(R.id.tb_alarm_status);
                final ImageView ivDeleteAlarm = (ImageView) convertView.findViewById(R.id.iv_alarm_delete);
                final LinearLayout llDaysOfWeek = (LinearLayout) convertView.findViewById(R.id.ll_days_of_week);

                final LinearLayout llSun = (LinearLayout) convertView.findViewById(R.id.ll_sun);
                final LinearLayout llMon = (LinearLayout) convertView.findViewById(R.id.ll_mon);
                final LinearLayout llTue = (LinearLayout) convertView.findViewById(R.id.ll_tue);
                final LinearLayout llWed = (LinearLayout) convertView.findViewById(R.id.ll_wed);
                final LinearLayout llThu = (LinearLayout) convertView.findViewById(R.id.ll_thu);
                final LinearLayout llFri = (LinearLayout) convertView.findViewById(R.id.ll_fri);
                final LinearLayout llSat = (LinearLayout) convertView.findViewById(R.id.ll_sat);

                final ProgressBar pbSun = (ProgressBar) convertView.findViewById(R.id.pb_sun);
                final ProgressBar pbMon = (ProgressBar) convertView.findViewById(R.id.pb_mon);
                final ProgressBar pbTue = (ProgressBar) convertView.findViewById(R.id.pb_tue);
                final ProgressBar pbWed = (ProgressBar) convertView.findViewById(R.id.pb_wed);
                final ProgressBar pbThu = (ProgressBar) convertView.findViewById(R.id.pb_thu);
                final ProgressBar pbFri = (ProgressBar) convertView.findViewById(R.id.pb_fri);
                final ProgressBar pbSat = (ProgressBar) convertView.findViewById(R.id.pb_sat);
                ivDeleteAlarm.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            DeleteAlarm(alarm, position);
                        } else {
                            RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
                        }
                    }
                });
                if (alarm.alarmDaysRepeating.contains("Sun")) {
                    pbSun.setProgress(1);
                } else {
                    pbSun.setProgress(0);
                }
                if (alarm.alarmDaysRepeating.contains("Mon")) {
                    pbMon.setProgress(1);
                } else {
                    pbMon.setProgress(0);
                }
                if (alarm.alarmDaysRepeating.contains("Tue")) {
                    pbTue.setProgress(1);
                } else {
                    pbTue.setProgress(0);
                }
                if (alarm.alarmDaysRepeating.contains("Wed")) {
                    pbWed.setProgress(1);
                } else {
                    pbWed.setProgress(0);
                }
                if (alarm.alarmDaysRepeating.contains("Thu")) {
                    pbThu.setProgress(1);
                } else {
                    pbThu.setProgress(0);
                }
                if (alarm.alarmDaysRepeating.contains("Fri")) {
                    pbFri.setProgress(1);
                } else {
                    pbFri.setProgress(0);
                }
                if (alarm.alarmDaysRepeating.contains("Sat")) {
                    pbSat.setProgress(1);
                } else {
                    pbSat.setProgress(0);
                }
                String sHour = alarm.alarmStartHour.toString();
                String sMinute = alarm.alarmStartMinute.toString();
                if (sMinute.length() == 1) {
                    sMinute = "0" + sMinute;
                }
                String startAMPM;
                if (alarm.alarmStartHour < 12) {
                    startAMPM = "AM";
                } else {
                    if (alarm.alarmStartHour >= 12) {
                        if (alarm.alarmStartHour == 12) {
                            sHour = "12";
                        } else {
                            sHour = String.valueOf(alarm.alarmStartHour - 12);
                        }
                        startAMPM = "PM";
                    } else {
                        startAMPM = "PM";
                    }

                }

                tvStartTime.setText(sHour + ":" + sMinute + " " + startAMPM);

                tvStartTime.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            startHour = alarm.alarmStartHour;
                            startMinute = alarm.alarmStartMinute;
                            TimeAlert(true, position);
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }

                    }
                });


                tvDuration.setText(alarm.alarmLength + " hours");
                sbDuration.setProgress((int) Double.parseDouble(alarm.alarmLength) * 2);
                sbDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        if (!Util.isMyServiceRunning(getActivity())) {
                            tvDuration.setText(String.valueOf((double) progress / 2) + " hours");
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    false
                                    , tbStatus.isChecked());
                        }
                    }
                });
                if (alarm.alarmStatus) {
                    tbStatus.setChecked(true);
                } else {
                    tbStatus.setChecked(false);
                }
                if (alarm.alarmRepeating) {
                    llDaysOfWeek.setVisibility(View.VISIBLE);
                    cbSetRepeating.setChecked(true);
                } else {
                    llDaysOfWeek.setVisibility(View.GONE);
                    cbSetRepeating.setChecked(false);
                }
                tbStatus.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            alarm.alarmLength = String.valueOf((double) sbDuration.getProgress() / 2);
                            alarm.alarmRepeating = cbSetRepeating.isChecked();
                            alarm.alarmAlwaysOn = false;
                            alarm.alarmStatus = isChecked;
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, alarm.alarmLength, alarm.alarmRepeating,
                                    alarm.alarmDaysRepeating, alarm.alarmAlwaysOn, alarm.alarmStatus);
                            if (isChecked) {
                                SetAlarms(alarm);
                            } else {
                                DestroyAlarms(alarm);

                            }
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                            buttonView.setChecked(!isChecked);
                        }


                    }

                });

                cbSetRepeating.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (isChecked) {
                                llDaysOfWeek.setVisibility(View.VISIBLE);
                            } else {

                                llDaysOfWeek.setVisibility(View.GONE);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                            buttonView.setChecked(!isChecked);
                        }
                    }

                });
                llSun.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbSun.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Sunday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Sunday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Sunday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Sunday", "");
                                }
                                pbSun.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Sunday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Sunday";
                                }
                                pbSun.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }

                    }
                });
                llMon.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbMon.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Monday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Monday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Monday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Monday", "");
                                }
                                pbMon.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Monday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Monday";
                                }
                                pbMon.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    //                            cbAlwaysOn.isChecked()
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }


                    }
                });
                llTue.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbTue.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Tuesday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Tuesday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Tuesday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Tuesday", "");
                                }
                                pbTue.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Tuesday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Tuesday";
                                }
                                pbTue.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    //                            cbAlwaysOn.isChecked()
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }


                    }
                });
                llWed.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbWed.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Wednesday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Wednesday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Wednesday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Wednesday", "");
                                }
                                pbWed.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Wednesday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Wednesday";
                                }
                                pbWed.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    //                            cbAlwaysOn.isChecked()
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }


                    }
                });
                llThu.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbThu.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Thursday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Thursday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Thursday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Thursday", "");
                                }
                                pbThu.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Thursday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Thursday";
                                }
                                pbThu.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    //                            cbAlwaysOn.isChecked()
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }


                    }
                });
                llFri.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbFri.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Friday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Friday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Friday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Friday", "");
                                }
                                pbFri.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Friday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Friday";
                                }
                                pbFri.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    //                            cbAlwaysOn.isChecked()
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }


                    }
                });
                llSat.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (!Util.isMyServiceRunning(getActivity())) {
                            if (pbSat.getProgress() == 1) {
                                if (alarm.alarmDaysRepeating.contains("|Saturday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("|Saturday", "");
                                } else if (alarm.alarmDaysRepeating.contains("Saturday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating.replace("Saturday", "");
                                }
                                pbSat.setProgress(0);
                            } else {
                                if (!alarm.alarmDaysRepeating.contains("Saturday")) {
                                    alarm.alarmDaysRepeating = alarm.alarmDaysRepeating + "|Saturday";
                                }
                                pbSat.setProgress(1);
                            }
                            tbStatus.setChecked(false);
                            UpdateAlarm(Long.valueOf(alarm.alarmID), alarm.alarmStartHour, alarm.alarmStartMinute, String.valueOf((double) sbDuration.getProgress() / 2), cbSetRepeating.isChecked(),
                                    alarm.alarmDaysRepeating,
                                    //                            cbAlwaysOn.isChecked()
                                    false
                                    , tbStatus.isChecked());
                        } else {
                            RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        }


                    }
                });
            }
            return convertView;
        }
    }

}
