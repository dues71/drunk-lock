package com.duesmobile.receivers;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.duesmobile.util.Util;

public class Receiver_Device_Admin extends DeviceAdminReceiver {
    void showToast(Context context, String msg) {
        //  String status = "Default Status";
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        Util.CreateGAEvent(context, "admin_event", "admin_enabled");
        showToast(context, "Admin Privileges Enabled! You can revoke privileges in basic settings.");
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return "You are about to Disable";
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        Util.CreateGAEvent(context, "admin_event", "admin_disabled");
        showToast(context, "Admin Privileges Disabled! You can activate privileges on the home screen.");
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent) {
        showToast(context, "Password Changed");
    }
}