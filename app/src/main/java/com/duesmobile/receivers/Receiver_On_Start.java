package com.duesmobile.receivers;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.duesmobile.services.Service_Drunk_Lock;
import com.duesmobile.util.Util;
import com.duesmobile.widget.ToggleWidgetProvider;

public class Receiver_On_Start extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            final SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            //Log.e("startup", "" + prefs.getBoolean("startOnBootup", false));
            Intent alarmTwoAMService = new Intent("com.duesmobile.services.Service_Alarm_Two_AM");
            context.startService(alarmTwoAMService);
            if (prefs.getBoolean(Util.KEY_ON_STARTUP, false)) {
                ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                for (RunningServiceInfo service : manager
                        .getRunningServices(Integer.MAX_VALUE)) {
                    if (!Service_Drunk_Lock.class.getName().equals(
                            service.service.getClassName())) {
                        Intent mainService = new Intent("com.duesmobile.services.Service_Drunk_Lock_Pro");
                        Intent i = new Intent(context, ToggleWidgetProvider.class);
                        i.setAction(ToggleWidgetProvider.UPDATE_TOGGLE_OFF);
                        context.sendBroadcast(i);
                        context.startService(mainService);
                    }
                }
            }
        }
    }
}