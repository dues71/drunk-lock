package com.duesmobile.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.duesmobile.ui.Activity_Dialog;
import com.duesmobile.util.DrunkLockDB;
import com.duesmobile.util.Util;

import java.lang.reflect.Method;

import telephony.ITelephony;

public class Receiver_Phone extends BroadcastReceiver {
    private static final String INTENT_PHONE_NUMBER = "android.intent.extra.PHONE_NUMBER";
    private boolean isRinging = false;
    private boolean dialogShown = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        // Log.v(TAG, "Receiving....");

        String phoneNumber = intent.getExtras().getString(Receiver_Phone.INTENT_PHONE_NUMBER);
        TelephonyManager telephony2 = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String outgoingNumber;
        String myNum;
        try {
            if (phoneNumber != null) {
                editor.putString("phoneNumber", phoneNumber);
                editor.putLong("saveTime", System.currentTimeMillis());
                editor.commit();
            }
        } catch (Exception e1) {
            // e1.printStackTrace();
        }
        try {
            outgoingNumber = String.valueOf(Integer.valueOf(preferences.getString("phoneNumber", "")));
            myNum = String.valueOf(Integer.valueOf(telephony2.getLine1Number()));
        } catch (NumberFormatException e1) {
            // Log.e("Error", "Error");
            outgoingNumber = preferences.getString("phoneNumber", "");
            myNum = telephony2.getLine1Number();
        }
        if (!preferences.getBoolean(Util.KEY_CONTACT_BLOCK_ALL, true)) {
            if (isBlockedNumber(context, outgoingNumber)) {
                if (preferences.getLong("saveTime", System.currentTimeMillis()) + 5000 <= System.currentTimeMillis()) {
                    editor.putString("phoneNumber", "");
                    editor.commit();
                    // Log.e("test","time");
                    dialogShown = false;
                } else {
                    try {
                        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra("state"))) {
                            isRinging = true;

                        }

                        if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(intent.getStringExtra("state"))) {
                            if (!isRinging) {
                                TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE); // Enable
                                // Contact
                                // Specific
                                // Blocking

                                try {

                                    Class<?> c = Class.forName(telephony.getClass().getName());
                                    Method m = c.getDeclaredMethod("getITelephony");
                                    m.setAccessible(true);
                                    ITelephony telephonyService = (ITelephony) m.invoke(telephony);
                                    telephonyService.endCall();
                                    editor.putString("phoneNumber", "");
                                    editor.commit();
                                    Util.blockType = "Drunk Lock has blocked the phone call that you were trying to make.";
                                    Intent in = new Intent(context, Activity_Dialog.class);
                                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(in);
                                    dialogShown = true;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                isRinging = false;
                            }
                        }

                    } catch (Exception e) {
                        // e.printStackTrace();
                    }
                }
            }

        } else {

            if (!outgoingNumber.equals("911") && !outgoingNumber.equals("0") && !outgoingNumber.equals("999") && !outgoingNumber.equals("112") && !outgoingNumber.equals("116") && !outgoingNumber.equals("311")
                    && !outgoingNumber.equals("000") && !outgoingNumber.equals(myNum)) {
                if (preferences.getLong("saveTime", System.currentTimeMillis()) + 5000 <= System.currentTimeMillis()) {
                    editor.putString("phoneNumber", "");
                    editor.commit();
                } else {
                    try {
                        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra("state"))) {
                            isRinging = true;

                        }

                        if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(intent.getStringExtra("state"))) {
                            if (!isRinging) {
                                TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                                try {

                                    Class<?> c = Class.forName(telephony.getClass().getName());
                                    Method m = c.getDeclaredMethod("getITelephony");
                                    m.setAccessible(true);
                                    ITelephony telephonyService = (ITelephony) m.invoke(telephony);
                                    telephonyService.endCall();
                                    editor.putString("phoneNumber", "");
                                    editor.commit();
                                    Util.blockType = "Drunk Lock has blocked the phone call that you were trying to make.";
                                    Intent i = new Intent(context, Activity_Dialog.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(i);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                isRinging = false;
                            }
                        }

                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }

    Boolean isBlockedNumber(Context mContext, String number) {

        DrunkLockDB db = new DrunkLockDB(mContext);
        db.open();
        Cursor c = db.getAllContacts();
        while (c.moveToNext()) {
            String blockNumber = c.getString(2);
            if (blockNumber != null) {
                blockNumber = blockNumber.replace("+", "");
                blockNumber = blockNumber.replace(" ", "");
                blockNumber = blockNumber.replace("-", "");
                blockNumber = blockNumber.replace("(", "");
                blockNumber = blockNumber.replace(")", "");
                blockNumber = blockNumber.replace(".", "");
                blockNumber = blockNumber.replace("*", "");
                blockNumber = blockNumber.replace(",", "");
                blockNumber = blockNumber.replace(";", "");
                if (blockNumber.equals(number)) {
                    return true;
                }
            }
        }
        db.close();
        return false;
    }

}
