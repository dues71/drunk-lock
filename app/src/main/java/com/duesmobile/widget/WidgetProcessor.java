package com.duesmobile.widget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.duesmobile.services.Service_Drunk_Lock;
import com.duesmobile.ui.Activity_Dialog;
import com.duesmobile.util.Util;

public class WidgetProcessor extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent mainService = new Intent(WidgetProcessor.this, Service_Drunk_Lock.class);
        Intent dialogIntent = new Intent(WidgetProcessor.this, Activity_Dialog.class);
        dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.e("action", getIntent().getAction());
        if (getIntent().getAction().equals(ToggleWidgetProvider.UPDATE_TOGGLE_ON)) {
            if (Util.isMyServiceRunning(WidgetProcessor.this)) {
                startActivity(dialogIntent);
            }

            finish();
        }
        if (getIntent().getAction().equals(ToggleWidgetProvider.UPDATE_TOGGLE_OFF)) {
            if (Util.isMyServiceRunning(WidgetProcessor.this)) {
                Intent i = new Intent(WidgetProcessor.this, ToggleWidgetProvider.class);
                i.setAction(ToggleWidgetProvider.UPDATE_TOGGLE_OFF);
                sendBroadcast(i);
            } else {
                startService(mainService);
                Intent i = new Intent(WidgetProcessor.this, ToggleWidgetProvider.class);
                i.setAction(ToggleWidgetProvider.UPDATE_TOGGLE_OFF);
                sendBroadcast(i);
            }

            finish();

        }
    }

}
