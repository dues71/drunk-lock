package com.duesmobile.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.duesmobile.R;

public class ToggleWidgetProvider extends AppWidgetProvider {


    public static final String UPDATE_TOGGLE_OFF = "ToggleOff";
    public static final String UPDATE_TOGGLE_ON = "ToggleOn";

    @Override
    public void onEnabled(Context context) {
        // TODO Auto-generated method stub
        updateWidget(context);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // TODO Auto-generated method stub
        updateWidget(context);
    }

    @Override
    public void onDisabled(Context context) {
        // TODO Auto-generated method stub
        updateWidget(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.toggle_widget);
        Log.e("toggle_widget", "Widget onReceive");
        //UPDATE_TOGGLE_ON Action
        if (UPDATE_TOGGLE_ON.equals(intent.getAction())) {
            views.setViewVisibility(R.id.btn_toggleOff, View.VISIBLE);
            views.setViewVisibility(R.id.btn_toggleOn, View.GONE);
            //	Log.e("toggle_widget","on");
            ComponentName widget = new ComponentName(context, ToggleWidgetProvider.class);
            AppWidgetManager.getInstance(context).updateAppWidget(widget, views);

        }
        //UPDATE_TOGGLE_OFF Action
        if (intent.getAction().equals(UPDATE_TOGGLE_OFF)) {
            views.setViewVisibility(R.id.btn_toggleOn, View.VISIBLE);
            views.setViewVisibility(R.id.btn_toggleOff, View.GONE);
            //Log.e("toggle_widget","off");
            ComponentName widget = new ComponentName(context, ToggleWidgetProvider.class);
            AppWidgetManager.getInstance(context).updateAppWidget(widget, views);

        }

        updateWidget(context);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // TODO Auto-generated method stub

        updateWidget(context);

    }

    void updateWidget(Context context) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.toggle_widget);
        Log.e("toggle_widget", "Widget onUpdate");
        //Update tip
        //	views.setViewVisibility(R.id.btn_toggleOn, 0);
        //Update split
        //	views.setViewVisibility(R.id.btn_toggleOn, 1);


        //Set Amount UPDATE_TOGGLE_ON
        Intent on = new Intent(context, WidgetProcessor.class);
        on.setAction(UPDATE_TOGGLE_ON);
        PendingIntent onPendingIntent = PendingIntent.getActivity(context, 0, on, 0);
        views.setOnClickPendingIntent(R.id.btn_toggleOn, onPendingIntent);
        //Set Tip UPDATE_TOGGLE_OFF
        Intent off = new Intent(context, WidgetProcessor.class);
        off.setAction(UPDATE_TOGGLE_OFF);
        PendingIntent offPendingIntent = PendingIntent.getActivity(context, 0, off, 0);
        views.setOnClickPendingIntent(R.id.btn_toggleOff, offPendingIntent);


        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName thisWidget = new ComponentName(context, ToggleWidgetProvider.class);
        if (appWidgetManager != null) {
            appWidgetManager.updateAppWidget(thisWidget, views);
        }

    }
}