package com.duesmobile.services;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;


public class Service_Starter extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }


    public void onCreate() {
        ActivityManager manager = (ActivityManager) Service_Starter.this.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (!Service_Drunk_Lock.class.getName().equals(
                    service.service.getClassName())) {
                Intent mainService = new Intent(this, Service_Drunk_Lock.class);

                startService(mainService);
            }
        }
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }
} 