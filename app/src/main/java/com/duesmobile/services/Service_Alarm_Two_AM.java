package com.duesmobile.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

import java.util.Calendar;

public class Service_Alarm_Two_AM extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    public void onCreate() {

        ManageAlarmTwoAM();

        stopSelf();

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO for communication return IBinder implementation
        return null;
    }

    void ManageAlarmTwoAM() {
        try {
            final SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this);
            if (prefs.getBoolean("after_two", false)) {
                Intent myIntent = new Intent(Service_Alarm_Two_AM.this,
                        Service_Starter.class);
                AlarmManager alarmManager = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);
                PendingIntent pendingIntent = PendingIntent.getService(this, 0,
                        myIntent, 0);

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 2);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                alarmManager.setRepeating(AlarmManager.RTC,
                        calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY,
                        24 * 60 * 60 * 1000, pendingIntent);
                // stop DrunkLock
                Intent myIntent2 = new Intent(Service_Alarm_Two_AM.this,
                        Service_Terminator.class);
                AlarmManager alarmManager2 = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);
                PendingIntent pendingIntent2 = PendingIntent.getService(this,
                        0, myIntent2, 0);

                Calendar calendar2 = Calendar.getInstance();
                calendar2.set(Calendar.HOUR_OF_DAY, 5);
                calendar2.set(Calendar.MINUTE, 0);
                calendar2.set(Calendar.SECOND, 0);

                alarmManager2
                        .setRepeating(AlarmManager.RTC,
                                calendar2.getTimeInMillis()
                                        + AlarmManager.INTERVAL_DAY,
                                24 * 60 * 60 * 1000, pendingIntent2
                        );

            } else {

                AlarmManager AmSC = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);

                Intent updateStartCancelIntent = new Intent(
                        Service_Alarm_Two_AM.this, Service_Starter.class);
                PendingIntent pendingStartCancelIntent = PendingIntent
                        .getService(this, 0, updateStartCancelIntent, 0);
                AlarmManager AmEC = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);

                Intent updateEndCancelIntent = new Intent(
                        Service_Alarm_Two_AM.this, Service_Starter.class);
                PendingIntent pendingEndCancelIntent = PendingIntent
                        .getService(this, 0, updateEndCancelIntent, 0);
                // Cancel alarms
                try {
                    AmSC.cancel(pendingStartCancelIntent);
                    AmEC.cancel(pendingEndCancelIntent);
                } catch (Exception e) {
                    // Log.e(TAG, "AlarmManager update was not canceled. " +
                    // e.toString());
                }

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}