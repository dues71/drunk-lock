package com.duesmobile.services;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Telephony;

import com.duesmobile.R;
import com.duesmobile.receivers.Receiver_Phone;
import com.duesmobile.ui.Activity_Dialog;
import com.duesmobile.util.DrunkLockDB;
import com.duesmobile.util.Util;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

@SuppressWarnings("ResourceType")
public class Service_Drunk_Lock extends Service {
    final private static int SIMPLE_NOTFICATION_ID = 1181;
    private boolean hasStarted = false;
    private ScreenReceiver myScreenReceiver = null;
    private Receiver_Phone receiver;
    private boolean wasScreenOn = true;
    private Context cx;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        // code to execute when the service is first created
        StartServiceItems();

    }

    @Override
    public void onDestroy() {
        // code to execute when the service is shutting down
        StopServiceItems();
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefsEdit = prefs.edit();
        prefsEdit.putBoolean(Util.KEY_ON_STARTUP, false);
        prefsEdit.commit();
    }


    @SuppressWarnings("deprecation")
    void StartServiceItems() {
        hasStarted = true;

        startForeground(R.string.app_name, new Notification());
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefsEdit = prefs.edit();
        prefsEdit.putBoolean(Util.KEY_ON_STARTUP, true);
        prefsEdit.commit();
        // Register Phone Blocking Receivers
        receiver = new Receiver_Phone();
        IntentFilter filter1 = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(receiver, filter1);
        // Code to Start the App Blocker
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        myScreenReceiver = new ScreenReceiver();
        registerReceiver(myScreenReceiver, filter);
        cx = Service_Drunk_Lock.this;
        SuperAppBlocker();
        // End App Blocker
        // Create Foreground Notification
        final Notification notificationDrunkLock = new Notification(R.drawable.ic_action_add, getResources().getString(R.string.app_name) + " is Enabled!", System.currentTimeMillis());
        Intent notifyIntent = new Intent(Service_Drunk_Lock.this, Activity_Dialog.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(Service_Drunk_Lock.this, 0, notifyIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationDrunkLock.flags = Notification.FLAG_ONGOING_EVENT;
        notificationDrunkLock.setLatestEventInfo(Service_Drunk_Lock.this, getResources().getString(R.string.app_name),
                "Click here to unlock!",
                pendingIntent);

        startForeground(SIMPLE_NOTFICATION_ID, notificationDrunkLock);

    }

    void StopServiceItems() {
        hasStarted = false;
        wasScreenOn = false;
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Unregister Call Blocking
        unregisterReceiver(receiver);

        if (prefs.getBoolean("applicationBlockOn", true)) {
            unregisterReceiver(myScreenReceiver);
        }

        // Put Code Here To Start

        stopForeground(true);
    }

    void SuperAppBlocker() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final ArrayList<String[]> blockedApplicationList = new ArrayList<String[]>();
        if (prefs.getBoolean("applicationBlockOn", true)) {
            DrunkLockDB db = new DrunkLockDB(this);
            db.open();
            Cursor c = db.getAllApplications();
            while (c.moveToNext()) {
                String[] application = {c.getString(0), c.getString(1), c.getString(2)};
                blockedApplicationList.add(application);
            }
            db.close();
        }
        Executors.newSingleThreadExecutor().execute(new Runnable() {

            @Override
            public void run() {
                while (wasScreenOn) {
                    try {
                        ActivityManager am = (ActivityManager) Service_Drunk_Lock.this.getSystemService(ACTIVITY_SERVICE);
                        // The first in the list of RunningTasks is always the foreground task.
                        RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);

                        String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();
                        PackageManager pm = Service_Drunk_Lock.this.getPackageManager();
                        PackageInfo foregroundAppPackageInfo = null;
                        try {
                            foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, PackageManager.GET_PERMISSIONS);
                        } catch (NameNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        String foregroundPackageName = foregroundAppPackageInfo.applicationInfo.packageName;
                        final String applicationName = (String) (foregroundAppPackageInfo.applicationInfo != null ? pm.getApplicationLabel(foregroundAppPackageInfo.applicationInfo) : "(unknown)");
                        String[] requestedPermissions = foregroundAppPackageInfo.requestedPermissions;
                        boolean smsBlocked = false;
                        if (prefs.getBoolean(Util.KEY_BLOCK_SMS, true)) {
                            if (!foregroundPackageName.equals("com.google.android.googlequicksearchbox")) {
                                boolean kitKatBlocked = false;
                                // KITKAT - if the Current App is the default SMS app, then block it
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                                    String defaultPackage = Telephony.Sms.getDefaultSmsPackage(Service_Drunk_Lock.this);
                                    if (foregroundPackageName.equals(defaultPackage)) {
                                        BlockApp(applicationName, foregroundPackageName);
                                        smsBlocked = true;
                                        kitKatBlocked = true;
                                    }
                                }
                                // If the Current App is not the Default SMS app, check for RECEIVE & SEND SMS PERMISSIONS
                                if (!kitKatBlocked) {
                                    boolean receiveSMS = false;
                                    boolean sendSMS = false;
                                    for (String requestedPermission : requestedPermissions) {
                                        if (requestedPermission.equals("android.permission.RECEIVE_SMS")) {
                                            receiveSMS = true;
                                        } else if (requestedPermission.equals("android.permission.SEND_SMS")) {
                                            sendSMS = true;
                                        }
                                    }
                                    if (receiveSMS && sendSMS && !foregroundPackageName.equals("com.android.phone")) {
                                        BlockApp(applicationName, foregroundPackageName);
                                        smsBlocked = true;
                                    }

                                }
                            }
                        }

                        if ((prefs.getBoolean("applicationBlockOn", true)) && !smsBlocked) {
                            for (String[] currentBlockedApp : blockedApplicationList) {
                                if (foregroundPackageName.equals(currentBlockedApp[2])) {
                                    BlockApp(currentBlockedApp[1], foregroundPackageName);
                                }
                            }
                        }
                    } catch (Exception ignored) {
                    }
                    try {
                        Thread.sleep(250);
                    } catch (Exception ignored) {
                    }
                }
            }
        });
    }


    private void BlockApp(String appName, String packageName) {

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

        Intent intent = new Intent(Service_Drunk_Lock.this, Activity_Dialog.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        Util.blockType = getResources().getString(R.string.app_name)+ " has blocked " + appName + " from starting.";
        startActivity(intent);
        // Kill Background Processes
        try {
            ActivityManager am = (ActivityManager) cx.getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                am.killBackgroundProcesses(packageName);
            }
        } catch (Exception ignored) {
        }
    }

    class ScreenReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                wasScreenOn = false;
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                wasScreenOn = true;
                cx = context;
                SuperAppBlocker();

            }

        }

    }

}