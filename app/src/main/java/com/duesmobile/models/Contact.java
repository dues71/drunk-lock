package com.duesmobile.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Contact implements Serializable {

    @SerializedName("ID")
    private String contactID;

    @SerializedName("position")
    private int contactPosition;

    @SerializedName("Name")
    private String contactName;

    @SerializedName("Phone")
    private String contactPhone;

    @SerializedName("PhotoID")
    private String photoID;

    @SerializedName("LastUpdate")
    private long lastUpdated;

    public Contact() {
        contactID = null;
        contactPosition = 0;
        contactName = null;
        contactPhone = null;
        lastUpdated = 0;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    public int getContactPosition() {
        return contactPosition;
    }

    public void setContactPosition(int contactPosition) {
        this.contactPosition = contactPosition;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
