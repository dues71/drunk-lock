package com.duesmobile.ui;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.duesmobile.R;
import com.duesmobile.receivers.Receiver_Device_Admin;
import com.duesmobile.services.Service_Drunk_Lock;
import com.duesmobile.services.Service_Terminator;
import com.duesmobile.util.Util;
import com.duesmobile.widget.ToggleWidgetProvider;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Calendar;


@SuppressWarnings("unused")
public class Fragment_Main extends Fragment {
    public static Intent mainService;
    public static ImageView ivDrunkLockToggle;
    public static LinearLayout llAdmin;
    public static int REQUEST_ENABLE = 17;
    View mainView;
    DevicePolicyManager mDPM;
    ComponentName mAdminName;

    // public static boolean closingService = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_main, container, false);
        setHasOptionsMenu(true);
        mDPM = (DevicePolicyManager) getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminName = new ComponentName(getActivity(), Receiver_Device_Admin.class);
        Util.CreateGAScreenView(getActivity(), "Home Screen");

        Util.questionCount = 1;
        ivDrunkLockToggle = (ImageView) mainView.findViewById(R.id.iv_drunk_lock_toggle);
        llAdmin = (LinearLayout) mainView.findViewById(R.id.ll_admin);
        if (!mDPM.isAdminActive(mAdminName)) {
            llAdmin.setVisibility(View.VISIBLE);
        }
        // Initialize Toggle Button
        if (Util.isMyServiceRunning(getActivity())) {
            ivDrunkLockToggle.setBackgroundResource(R.drawable.full_glass_tall);
        } else {
            ivDrunkLockToggle.setBackgroundResource(R.drawable.empty_glass_tall);
        }

        try {
            // Initialize AdMob View
            AdView adView = (AdView) mainView.findViewById(R.id.adView);
            AdRequest.Builder adBuilder = new AdRequest.Builder();
            adBuilder.addKeyword("Drunk");
            adBuilder.addKeyword("Alcohol");
            adBuilder.addKeyword("Games");

            adBuilder.addKeyword("Bar");
            adBuilder.addKeyword("Taxi");
            AdRequest adRequest = adBuilder.build();
            adView.loadAd(adRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mainService = new Intent(getActivity(), Service_Drunk_Lock.class);
        llAdmin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (!mDPM.isAdminActive(mAdminName)) {
                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                            mAdminName);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                            "Drunk Lock is asking to be an active device administrator to prevent it from being force " +
                                    "stopped while active. When activated, you will be easily able to revoke access by going to basic settings."
                    );
                    startActivityForResult(intent, REQUEST_ENABLE);
                }


            }
        });
        ivDrunkLockToggle.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                ivDrunkLockToggle.setBackgroundResource(R.drawable.full_glass_tall);
                if (Util.isMyServiceRunning(getActivity())) {
                    Intent i = new Intent(getActivity(), Activity_Dialog.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getActivity(), ToggleWidgetProvider.class);
                    i.setAction(ToggleWidgetProvider.UPDATE_TOGGLE_OFF);
                    getActivity().sendBroadcast(i);
                    SetEndAlarm();
                    getActivity().startService(mainService);
                }
            }
        });

        return mainView;
    }

    private void SetEndAlarm() {
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent endIntent = new Intent(getActivity(), Service_Terminator.class);
        Time curTime = new Time(Time.getCurrentTimezone());
        curTime.setToNow();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int endDay = curTime.weekDay + 1;
        int endHour = curTime.hour;
        int endMinute = curTime.minute;
        int iDur = prefs.getInt(Util.KEY_DURATION_PROGRESS, 0);
        String duration = String.valueOf((double) iDur / 2);
        if (iDur > 0) {
            int aMin;
            int aHour;
            if (duration.contains(".5")) {
                aMin = 30;
                aHour = Integer.valueOf(duration.substring(0, duration.indexOf(".")));
            } else if (duration.contains(".")) {
                aMin = 0;
                aHour = Integer.valueOf(duration.substring(0, duration.indexOf(".")));
            } else {
                aMin = 0;
                aHour = Integer.valueOf(duration);
            }
            endMinute = endMinute + aMin;
            endHour = endHour + aHour;
            if (endMinute >= 60) {
                endHour = endHour + 1;
                endMinute = endMinute - 60;
            }
            if (endHour >= 24) {
                endHour = endHour - 24;
                if (endDay + 1 > 7) {
                    endDay = 0;
                } else {
                    endDay = endDay + 1;
                }
            }
            // Create Single End Alarm
            PendingIntent pendingEndIntent = PendingIntent.getService(getActivity(), Integer.valueOf("999"), endIntent, 0);
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set(Calendar.DAY_OF_WEEK, endDay);
            endCalendar.set(Calendar.HOUR_OF_DAY, endHour);
            endCalendar.set(Calendar.MINUTE, endMinute);
            endCalendar.set(Calendar.SECOND, 0);
            alarmManager.set(AlarmManager.RTC,
                    endCalendar.getTimeInMillis(), pendingEndIntent);
        }
    }

    /* Called when the second activity's finished */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (REQUEST_ENABLE == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                llAdmin.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        // Initialize Toggle Button
        if (Util.isMyServiceRunning(getActivity())) {
            ivDrunkLockToggle.setBackgroundResource(R.drawable.full_glass_tall);
            // serviceStatus.setText("Service Started");
        } else {
            ivDrunkLockToggle.setBackgroundResource(R.drawable.empty_glass_tall);
        }
    }

}