package com.duesmobile.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.duesmobile.services.Service_Drunk_Lock;
import com.duesmobile.util.Util;
import com.duesmobile.widget.ToggleWidgetProvider;

import java.util.Random;

public class Activity_Dialog extends Activity {

    private static Context cxt;
    private static Activity activity;

    private static String[] getRandomEquation() {
        Random r = new Random(System.currentTimeMillis());
        boolean collegeMath = false;
        float largeNumber = (float) r.nextInt(30) + 1;
        float smallerNumber = (float) r.nextInt(20) + 1;
        float smallNumber = (float) r.nextInt(10) + 1;
        int signInt1 = r.nextInt(2);
        int signInt2 = r.nextInt(2);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(cxt);
        if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("average")) {
            largeNumber = (float) r.nextInt(50) + 4;
            smallerNumber = (float) r.nextInt(40) + 4;
            smallNumber = (float) r.nextInt(30) + 4;
            signInt1 = r.nextInt(3);
            signInt2 = r.nextInt(3);
        }
        if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("hard")) {
            largeNumber = (float) r.nextInt(60) + 8;
            smallerNumber = (float) r.nextInt(50) + 8;
            smallNumber = (float) r.nextInt(40) + 8;
            signInt1 = r.nextInt(3);
            signInt2 = r.nextInt(6);
        }
        if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("college")) {
            largeNumber = (float) r.nextInt(220) + 20;
            smallerNumber = (float) r.nextInt(31) + 9;
            smallNumber = (float) r.nextInt(70) + 9;
            signInt1 = r.nextInt(4);
            signInt2 = r.nextInt(9);
            collegeMath = true;
        }
        String sign1 = "+";
        String sign2 = "+";
        if (signInt1 == 0) {
            sign1 = "-";
        } else if (signInt1 == 1) {
            sign1 = "+";
        } else if (signInt1 == 2) {
            sign1 = "*";
        } else if (signInt1 == 3) {
            sign1 = "/";
        }

        if (signInt2 == 0) {
            sign2 = "-";
        } else if (signInt2 == 1) {
            sign2 = "+";
        } else if (signInt2 == 2) {
            sign2 = "*";
        } else if (signInt2 == 3) {
            sign2 = "/";
        } else if (signInt2 == 4) {
            sign2 = "*";
        } else if (signInt2 == 5) {
            sign2 = "/";
        } else if (signInt2 == 6) {
            sign2 = "√";
        } else if (signInt2 == 7) {
            sign2 = "²";
        } else if (signInt2 == 8) {
            sign2 = "³";
        }


        float answer = (float) -1.0;
        try {
            if (!collegeMath) {
                if (signInt1 == 0 && signInt2 == 0) {
                    answer = smallerNumber - largeNumber - smallNumber;
                }
                if (signInt1 == 1 && signInt2 == 0) {
                    answer = smallerNumber + largeNumber - smallNumber;
                }
                if (signInt1 == 2 && signInt2 == 0) {
                    answer = (smallerNumber * largeNumber) - smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 1) {
                    answer = smallerNumber - largeNumber + smallNumber;
                }
                if (signInt1 == 1 && signInt2 == 1) {
                    answer = smallerNumber + largeNumber + smallNumber;
                }
                if (signInt1 == 2 && signInt2 == 1) {
                    answer = (smallerNumber * largeNumber) + smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 2) {
                    answer = smallerNumber - (largeNumber * smallNumber);
                }
                if (signInt1 == 1 && signInt2 == 2) {
                    answer = smallerNumber + (largeNumber * smallNumber);
                }
                if (signInt1 == 2 && signInt2 == 2) {
                    answer = (smallerNumber * largeNumber) * smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 3) {
                    answer = smallerNumber - (largeNumber / smallNumber);
                }
                if (signInt1 == 1 && signInt2 == 3) {
                    answer = smallerNumber + (largeNumber / smallNumber);
                }
                if (signInt1 == 2 && signInt2 == 3) {
                    answer = (smallerNumber * largeNumber) / smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 4) {
                    answer = smallerNumber - (largeNumber * smallNumber);
                }
                if (signInt1 == 1 && signInt2 == 4) {
                    answer = smallerNumber + (largeNumber * smallNumber);
                }
                if (signInt1 == 2 && signInt2 == 4) {
                    answer = (smallerNumber * largeNumber) * smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 5) {
                    answer = smallerNumber - (largeNumber / smallNumber);
                }
                if (signInt1 == 1 && signInt2 == 5) {
                    answer = smallerNumber + (largeNumber / smallNumber);
                }
                if (signInt1 == 2 && signInt2 == 5) {
                    answer = (smallerNumber * largeNumber) / smallNumber;
                }
            } else {
                if (signInt1 == 0 && signInt2 == 4) {
                    answer = smallerNumber - (largeNumber * smallNumber);
                }
                if (signInt1 == 1 && signInt2 == 4) {
                    answer = smallerNumber + (largeNumber * smallNumber);
                }
                if (signInt1 == 2 && signInt2 == 4) {
                    answer = (smallerNumber * largeNumber) * smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 5) {
                    answer = smallerNumber - (largeNumber / smallNumber);
                }
                if (signInt1 == 1 && signInt2 == 5) {
                    answer = smallerNumber + (largeNumber / smallNumber);
                }
                if (signInt1 == 2 && signInt2 == 5) {
                    answer = (smallerNumber * largeNumber) / smallNumber;
                }
                if (signInt1 == 0 && signInt2 == 6) {
                    answer = largeNumber - (float) Math.sqrt((double) smallerNumber);
                }
                if (signInt1 == 1 && signInt2 == 6) {
                    answer = largeNumber + (float) Math.sqrt((double) smallerNumber);
                }
                if (signInt1 == 2 && signInt2 == 6) {
                    answer = largeNumber * (float) Math.sqrt((double) smallerNumber);
                }
                if (signInt1 == 0 && signInt2 == 7) {
                    answer = largeNumber - (float) Math.pow(smallerNumber, 2);
                }
                if (signInt1 == 1 && signInt2 == 7) {
                    answer = largeNumber + (float) Math.pow(smallerNumber, 2);
                }
                if (signInt1 == 2 && signInt2 == 7) {
                    answer = largeNumber * (float) Math.pow(smallerNumber, 2);
                }
                if (signInt1 == 0 && signInt2 == 8) {
                    answer = largeNumber - (float) Math.pow(smallerNumber, 3);
                }
                if (signInt1 == 1 && signInt2 == 8) {
                    answer = largeNumber + (float) Math.pow(smallerNumber, 3);
                }
                if (signInt1 == 2 && signInt2 == 8) {
                    answer = largeNumber * (float) Math.pow(smallerNumber, 3);
                }
            }
        } catch (Exception e) {
            answer = (float) -1.0;
            // e.printStackTrace();
        }
        //     Log.e("answer1", answer + "");
        answer = (float) Math.round(answer);
        //      Log.e("answer2", answer + "");
        // answer = (float) Math.round(answer);
        String a2 = String.valueOf(answer);
        if (a2.substring(a2.length() - 2, a2.length()).equals(".0")) {
            a2 = a2.substring(0, a2.length() - 2);
        }
        String equation;
        if (!collegeMath) {
            equation = String.valueOf(String.valueOf(smallerNumber).substring(0, String.valueOf(smallerNumber).length() - 2) + " " + sign1 + " "
                    + String.valueOf(largeNumber).substring(0, String.valueOf(largeNumber).length() - 2) + " " + sign2 + " "
                    + String.valueOf(smallNumber).substring(0, String.valueOf(smallNumber).length() - 2));
        } else {
            if (signInt2 == 6) {
                equation = String.valueOf(String.valueOf(largeNumber).substring(0, String.valueOf(largeNumber).length() - 2) + " " + sign1 + " "
                        + sign2 + String.valueOf(smallerNumber).substring(0, String.valueOf(smallerNumber).length() - 2));
            } else if (signInt2 == 7 || signInt2 == 8) {
                equation = String.valueOf(String.valueOf(largeNumber).substring(0, String.valueOf(largeNumber).length() - 2) + " " + sign1 + " "
                        + String.valueOf(smallerNumber).substring(0, String.valueOf(smallerNumber).length() - 2)) + sign2;
            } else {
                equation = String.valueOf(String.valueOf(smallerNumber).substring(0, String.valueOf(smallerNumber).length() - 2) + " " + sign1 + " "
                        + String.valueOf(largeNumber).substring(0, String.valueOf(largeNumber).length() - 2) + " " + sign2 + " "
                        + String.valueOf(smallNumber).substring(0, String.valueOf(smallNumber).length() - 2));
            }
        }
        return new String[]{equation, a2};
    }

    private static void randomMathDialog() {
        final InputMethodManager mgr = (InputMethodManager) cxt.getSystemService(Context.INPUT_METHOD_SERVICE);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(cxt);
        LinearLayout dialogLayout = new LinearLayout(cxt);
        boolean isPositive = false;
        String[] tempEquation1 = {"", ""};
        while (!isPositive) {
            String[] tempEquation2 = getRandomEquation();
            if (!tempEquation2[1].contains("-")) {
                tempEquation1 = tempEquation2;
                isPositive = true;
            }
        }
        final String[] equation = tempEquation1;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(cxt);
        if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("easy")) {
            alertBuilder.setMessage("Solve this equation:\n" + equation[0] + "\n(Answer will always be positive)");
        } else {
            alertBuilder.setMessage("Solve this equation:\n" + equation[0] + "\n(Answer is rounded to the nearest positive whole number)");
        }
        // Set an EditText view to get user input
        final EditText etAnswer = new EditText(cxt);
        etAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
        etAnswer.setHint("Type Answer here");
        etAnswer.setImeActionLabel("OK", EditorInfo.IME_ACTION_GO);
        // configure dialogLayout
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        mgr.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);
        dialogLayout.addView(etAnswer);
        alertBuilder.setView(dialogLayout);
        alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String answerInput = etAnswer.getText().toString();
                mgr.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);
                Util.questionCount = Util.questionCount + 1;
                if (answerInput.equals(equation[1])) {
                    if (Util.questionCount <= Util.numTimes) {
                        ResultDialog("Correct!\nAre you ready for the next question?", String.valueOf("Question #" + Util.questionCount), true, false);
                    } else {
                        ResultDialog("Congratulations, you were sober enough to pass!", String.valueOf("Unlock!"), true, true);
                    }

                } else {
                    // Start Creation of random Quote
                    Random randomNum = new Random(System.currentTimeMillis());
                    String quote;
                    int rand = randomNum.nextInt(70);
                    rand = rand + 10;
                    quote = "\n\n\"" + getStringResourceByName("quote" + String.valueOf(rand)) + "\"\n  -" + getStringResourceByName("quote" + String.valueOf(rand) + String.valueOf(rand));
                    // End Creation of random Quote

                    ResultDialog(String.valueOf("Wrong! The correct answer was " + equation[1]) + quote, String.valueOf("I'm Drunk"), false, true);

                }

            }
        });

        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mgr.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);
                dialog.dismiss();
                Util.questionCount = 1;
                Util.blockType = "Drunk Lock is blocking all outgoing calls and text messages.";
                activity.finish();
            }
        });
        alertBuilder.setCancelable(false);
        final AlertDialog alert = alertBuilder.create();
        alert.show();
        alert.getWindow().getAttributes();
        etAnswer.requestFocus();
        etAnswer.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView arg0, int actionId, KeyEvent arg2) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    InputMethodManager imm = (InputMethodManager) cxt.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);
                    String answerInput = etAnswer.getText().toString();
                    mgr.hideSoftInputFromWindow(etAnswer.getWindowToken(), 0);
                    Util.questionCount = Util.questionCount + 1;
                    if (answerInput.equals(equation[1])) {
                        if (Util.questionCount <= Util.numTimes) {
                            ResultDialog("Correct!\nAre you ready for the next question?", String.valueOf("Question #" + Util.questionCount), true, false);
                        } else {
                            ResultDialog("Congratulations, you were sober enough to pass!", String.valueOf("Unlock!"), true, true);
                        }

                    } else {
                        // Start Creation of random Quote
                        Random randomNum = new Random(System.currentTimeMillis());
                        String quote;
                        int rand = randomNum.nextInt(70);
                        rand = rand + 10;
                        quote = "\n\n\"" + getStringResourceByName("quote" + String.valueOf(rand)) + "\"\n  -" + getStringResourceByName("quote" + String.valueOf(rand) + String.valueOf(rand));
                        // End Creation of random Quote

                        ResultDialog(String.valueOf("Wrong! The correct answer was " + equation[1]) + quote, String.valueOf("I'm Drunk"), false, true);

                    }
                    alert.dismiss();
                    return true;
                }
                return false;
            }
        });
        mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private static String getStringResourceByName(String aString) {
        String packageName = activity.getPackageName();
        int resId = activity.getResources().getIdentifier(aString, "string", packageName);
        return activity.getString(resId);
    }

    private static void ResultDialog(final String titleText, String buttonText, boolean resultType, boolean isFinished) {
        cxt.getSystemService(Context.INPUT_METHOD_SERVICE);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(cxt);
        LinearLayout dialogLayout = new LinearLayout(cxt);
        alertBuilder.setMessage(titleText);

        // configure dialogLayout
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        if (resultType) {
            if (!isFinished) {
                alertBuilder.setView(dialogLayout);
                alertBuilder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.CreateGAEvent(cxt, "dialogItem", "Random_Math_Dialog");
                        randomMathDialog();
                    }
                });
            } else {
                dialogLayout.addView(Util.CreateShareButton(cxt, "I was sober enough to pass Drunk Lock!"));
                alertBuilder.setView(dialogLayout);
                Intent mainService = new Intent(activity, Service_Drunk_Lock.class);
                activity.stopService(mainService);
                alertBuilder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent i = new Intent(activity, ToggleWidgetProvider.class);
                        i.setAction(ToggleWidgetProvider.UPDATE_TOGGLE_ON);
                        activity.sendBroadcast(i);
                        dialog.dismiss();
                        Util.blockType = "Drunk Lock is blocking all outgoing calls and text messages.";
                        Util.CreateGAEvent(cxt, "dialogItem", "Success_Dialog");
                        activity.finish();
                    }
                });
            }

        } else {
            dialogLayout.addView(Util.CreateShareButton(cxt, "Drunk Lock has prevented me from using my phone!"));
            alertBuilder.setView(dialogLayout);
            alertBuilder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    Util.questionCount = 1;
                    Util.blockType = "Drunk Lock is blocking all outgoing calls and text messages.";
                    Util.CreateGAEvent(cxt, "dialogItem", "Fail_Dialog");
                    // Initialize Toggle Button
                    activity.finish();
                }
            });
        }
        alertBuilder.setCancelable(false);
        AlertDialog alert = alertBuilder.create();
        alert.show();
        alert.getWindow().getAttributes();
    }

    private static void StartDialog() {
        cxt.getSystemService(Context.INPUT_METHOD_SERVICE);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(cxt);
        LinearLayout dialogLayout = new LinearLayout(cxt);
        alertBuilder.setMessage(Util.blockType + "  You must answer a series of questions correctly in order disable Drunk Lock");
        Util.CreateGAEvent(cxt, "dialogItem", "Start_Dialog");
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        alertBuilder.setView(dialogLayout);
        alertBuilder.setPositiveButton(String.valueOf("Question #" + Util.questionCount), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                randomMathDialog();
            }
        });

        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                Util.questionCount = 1;
                Util.blockType = "Drunk Lock is blocking all outgoing calls and text messages.";
                activity.finish();
            }
        });
        alertBuilder.setCancelable(false);
        AlertDialog alert = alertBuilder.create();
        alert.show();
        alert.getWindow().getAttributes();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.questionCount = 1;
        Util.numTimes = 3;
        cxt = Activity_Dialog.this;
        activity = Activity_Dialog.this;
        Util.CreateGAScreenView(cxt, "Dialog Activity");
        StartDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (Util.activityMainStarted) {
                Util.activity.finish();
            }
        } catch (Exception ignored) {
        }

    }

}
