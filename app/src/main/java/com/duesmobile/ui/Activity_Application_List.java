package com.duesmobile.ui;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.duesmobile.R;
import com.duesmobile.util.DrunkLockDB;
import com.duesmobile.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;

@SuppressWarnings("ConstantConditions")
public class Activity_Application_List extends ListActivity {

    private Context cxt;
    private boolean isBlocked = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_list);
        ActionBar ab = getActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_actionbar)));
        ab.setTitle("Application List");


        // Initialize AdMob View
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest.Builder adBuilder = new AdRequest.Builder();
        adBuilder.addKeyword("Beer");
        adBuilder.addKeyword("Drunk");
        adBuilder.addKeyword("Alcohol");
        adBuilder.addKeyword("Food");
        adBuilder.addKeyword("Bar");
        adBuilder.addKeyword("Taxi");
        AdRequest adRequest = adBuilder.build();
        adView.loadAd(adRequest);
        Util.CreateGAScreenView(Activity_Application_List.this, "Application List");
        cxt = Activity_Application_List.this;
        final PackageManager pm = this.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        final ArrayList<ResolveInfo> list = (ArrayList<ResolveInfo>) pm.queryIntentActivities(intent, PackageManager.PERMISSION_GRANTED);
        Collections.sort(list, new ResolveInfo.DisplayNameComparator(pm));
        final ArrayAdapter<ResolveInfo> adapter = new ArrayAdapter<ResolveInfo>(this, R.layout.list_item_application, list) {
            @SuppressLint("NewApi")
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null)
                    convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_application, parent, false);
                DrunkLockDB db = new DrunkLockDB(cxt);
                db.open();
                Cursor c = db.getAllApplications();
                while (c.moveToNext()) {
                    if (c.getString(2).equals(list.get(position).activityInfo.applicationInfo.loadLabel(pm).toString())) {
                        isBlocked = true;
                    }

                }
                db.close();
                TextView tvLabel = (TextView) convertView.findViewById(R.id.tv_label);
                TextView tvPackageName = (TextView) convertView.findViewById(R.id.tv_packageName);
                ImageView ivLogo = (ImageView) convertView.findViewById(R.id.iv_logo);
                if (!isBlocked) {
                    final String label = list.get(position).activityInfo.applicationInfo.loadLabel(pm).toString();
                    tvLabel.setText(label);

                    final String packageName = list.get(position).activityInfo.applicationInfo.packageName;
                    tvPackageName.setText(packageName);

                    final Drawable logo = list.get(position).activityInfo.applicationInfo.loadIcon(pm);
                    ivLogo.setImageDrawable(logo);

                } else {
                    isBlocked = false;
                }
                return convertView;
            }
        };
        setListAdapter(adapter);

        getListView().setTextFilterEnabled(true);

        getListView().setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {

                    // Add the current selection to blockedApplicationList
                    TextView tvLabel = (TextView) view.findViewById(R.id.tv_label);
                    TextView tvPackageName = (TextView) view.findViewById(R.id.tv_packageName);
                    String packageName = tvPackageName.getText().toString();
                    String label = tvLabel.getText().toString();
                    if (!label.equals("")) {
                        if (!label.contains("Drunk Lock")) {
                            DrunkLockDB dbAdd = new DrunkLockDB(cxt);
                            dbAdd.open();
                            dbAdd.insertApplication(label, packageName);
                            dbAdd.close();
                            finish();
                        } else {
                            Toast.makeText(Activity_Application_List.this, "BlockCeption!! Drunk Lock can not Block Drunk Lock.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(Activity_Application_List.this, "You cannot select an empty row. Please scroll down in order to remove the rows.", Toast.LENGTH_LONG).show();
                    }
                    // Log.e("test", "" + packageName);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // On Item Click Activity
                // This is where I want to send the Package Name of the app
                // selected to be passed to a method.

            }
        });

    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

}
