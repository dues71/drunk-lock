package com.duesmobile.ui;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.duesmobile.R;
import com.duesmobile.util.DrunkLockDB;
import com.duesmobile.util.RepeatSafeToast;
import com.duesmobile.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("unused")
public class Fragment_Block_Settings extends ListFragment {


    // Capture third menu button click
    private final MenuItem.OnMenuItemClickListener AddApplicationClickListener = new MenuItem.OnMenuItemClickListener() {

        public boolean onMenuItemClick(MenuItem item) {
            if (!Util.isMyServiceRunning(getActivity())) {
                Intent intent = new Intent(getActivity(), Activity_Application_List.class);
                startActivity(intent);
            } else {
                RepeatSafeToast.show(getActivity(), "Changes can't be made while Drunk Lock Pro is active");
            }
            return false;
        }
    };
    private ApplicationListAdapter listAdapter;
    private ArrayList<String[]> blockedApplicationList = new ArrayList<String[]>();
    private SharedPreferences.Editor prefEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View blockSettingsView = inflater.inflate(R.layout.fragment_block_settings, container, false);
        setHasOptionsMenu(true);
        Util.CreateGAScreenView(getActivity(), "Block Settings");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefEdit = prefs.edit();
        if (blockSettingsView != null) {
            // Initialize view container
            CheckBox cbBlockCalls = (CheckBox) blockSettingsView.findViewById(R.id.cb_block_calls);
            CheckBox cbBlockSms = (CheckBox) blockSettingsView.findViewById(R.id.cb_block_sms);
            // Initialize AdMob View
            AdView adView = (AdView) blockSettingsView.findViewById(R.id.adView);
            AdRequest.Builder adBuilder = new AdRequest.Builder();
            adBuilder.addKeyword("Beer");
            adBuilder.addKeyword("Drunk");
            adBuilder.addKeyword("Alcohol");
            adBuilder.addKeyword("Food");
            adBuilder.addKeyword("Bar");
            adBuilder.addKeyword("Taxi");
            AdRequest adRequest = adBuilder.build();
            adView.loadAd(adRequest);

            // Initialize Views
            cbBlockCalls.setChecked(prefs.getBoolean(Util.KEY_BLOCK_CALLS, true));
            cbBlockSms.setChecked(prefs.getBoolean(Util.KEY_BLOCK_SMS, true));
            if (!Util.hasTelephony(getActivity())) {
                cbBlockCalls.setVisibility(View.GONE);
                prefEdit.putBoolean(Util.KEY_BLOCK_CALLS, false).commit();
            }
            // Add Listeners
            cbBlockCalls.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    if (!Util.isMyServiceRunning(getActivity())) {
                        if (buttonView.isChecked()) {
                            prefEdit.putBoolean(Util.KEY_BLOCK_CALLS, true).commit();
                        } else {
                            prefEdit.putBoolean(Util.KEY_BLOCK_CALLS, false).commit();
                        }
                    } else {
                        RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        buttonView.setChecked(!isChecked);
                    }
                }
            });
            cbBlockSms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    if (!Util.isMyServiceRunning(getActivity())) {
                        if (buttonView.isChecked()) {
                            prefEdit.putBoolean(Util.KEY_BLOCK_SMS, true).commit();
                        } else {
                            prefEdit.putBoolean(Util.KEY_BLOCK_SMS, false).commit();
                        }
                    } else {
                        RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                        buttonView.setChecked(!isChecked);
                    }
                }
            });
        }

        return blockSettingsView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add("Add Application").setIcon(R.drawable.ic_action_add).setOnMenuItemClickListener(AddApplicationClickListener).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    void SaveApplicationBlockSettings() {

        if (Util.isMyServiceRunning(getActivity())) {
            Intent i = new Intent(getActivity(), Activity_Dialog.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Util.blockType = "You must disable DrunkLock in order to save your changes.";
            startActivity(i);
        }
    }

    void PopulateApplicationsList() {
        blockedApplicationList = new ArrayList<String[]>();
        DrunkLockDB db = new DrunkLockDB(getActivity());
        db.open();
        Cursor c = db.getAllApplications();
        while (c.moveToNext()) {
            String[] application = {c.getString(0), c.getString(1), c.getString(2)};
            blockedApplicationList.add(application);
        }
        db.close();
        listAdapter = new ApplicationListAdapter(getActivity(), R.layout.list_item_application, blockedApplicationList);
        setListAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        PopulateApplicationsList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Util.isMyServiceRunning(getActivity())) {
            return;
        }
        SaveApplicationBlockSettings();
    }

    public class ApplicationListAdapter extends ArrayAdapter<String[]> {

        public ApplicationListAdapter(Context context, int textViewResourceId, List<String[]> blockedApplicationList) {
            super(context, textViewResourceId, blockedApplicationList);
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_application, parent, false);
            }
            String[] application = blockedApplicationList.get(position);
            // Define Views
            if (convertView != null) {
                TextView tvLabel = (TextView) convertView.findViewById(R.id.tv_label);
                TextView tvPackageName = (TextView) convertView.findViewById(R.id.tv_packageName);
                ImageView ivLogo = (ImageView) convertView.findViewById(R.id.iv_logo);
                ImageView ivAppRemove = (ImageView) convertView.findViewById(R.id.iv_application_delete);

                ivAppRemove.setVisibility(View.VISIBLE);
                ivAppRemove.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        String[] application = blockedApplicationList.get(position);
                        DrunkLockDB dbRemove = new DrunkLockDB(getActivity());
                        dbRemove.open();
                        dbRemove.deleteApplication(Long.parseLong(application[0]));
                        dbRemove.close();
                        blockedApplicationList.remove(position);
                        listAdapter.notifyDataSetChanged();
                    }
                });
                tvLabel.setText(application[1]);
                tvPackageName.setText(application[2]);
                try {
                    Drawable icon = getActivity().getPackageManager().getApplicationIcon(application[2]);
                    ivLogo.setImageDrawable(icon);
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return convertView;
        }
    }

}