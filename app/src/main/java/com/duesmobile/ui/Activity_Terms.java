package com.duesmobile.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.duesmobile.R;
import com.duesmobile.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

@SuppressWarnings("unused")
public class Activity_Terms extends Activity {

    // Capture edit menu button click
    MenuItem.OnMenuItemClickListener UpgradeClickListener = new MenuItem.OnMenuItemClickListener() {

        public boolean onMenuItemClick(MenuItem item) {
            Util.OpenDrunkLockPro(Activity_Terms.this, "ad_clicked");
            return false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_legal);
        ActionBar ab = getActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_actionbar)));
        ab.setTitle("Terms & Conditions");
        // Initialize AdMob View
        AdView adView = (AdView) findViewById(R.id.adView);
//        adView.setAdUnitId("ca-app-pub-2872870925102353/8370314823");
//        adView.setAdSize(AdSize.SMART_BANNER);
        AdRequest.Builder adBuilder = new AdRequest.Builder();
        adBuilder.addKeyword("Beer");
        adBuilder.addKeyword("Drunk");
        adBuilder.addKeyword("Alcohol");
        adBuilder.addKeyword("Food");
        adBuilder.addKeyword("Bar");
        adBuilder.addKeyword("Taxi");
        AdRequest adRequest = adBuilder.build();
        adView.loadAd(adRequest);
        Util.CreateGAScreenView(Activity_Terms.this, "Legal Info");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add("Get Drunk Lock Pro").setOnMenuItemClickListener(UpgradeClickListener).setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;

        }

        return true;
    }
}