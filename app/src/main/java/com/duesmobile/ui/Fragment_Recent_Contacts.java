package com.duesmobile.ui;

import android.annotation.SuppressLint;
import android.app.ListFragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.duesmobile.R;
import com.duesmobile.models.Contact;
import com.duesmobile.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

import static android.provider.ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
import static android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
import static android.provider.ContactsContract.Contacts;

@SuppressWarnings("unused")
public class Fragment_Recent_Contacts extends ListFragment implements OnRefreshListener {
    private String TAG = "Fragment_Contacts_Added";
    private ArrayList<Contact> contactList;
    private LinearLayout llLoading;
    private LinearLayout llSort;
    private TextView tvNoResults;
    private TextView tvSortType;
    private LoadContactsList loadContactsList;
    private int mLastFirstVisibleItem;
    private ContactListAdapter mListAdapter;
    private Spinner spinnerSort;
    private PullToRefreshLayout mPullToRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contactsAddedView = inflater.inflate(R.layout.fragment_contacts_added, container, false);
        setHasOptionsMenu(true);
        Util.CreateGAScreenView(getActivity(), "Recent Contacts");
        if (contactsAddedView != null) {
            llLoading = (LinearLayout) contactsAddedView.findViewById(R.id.ll_loading);
            llSort = (LinearLayout) contactsAddedView.findViewById(R.id.ll_sort_bar);
            tvNoResults = (TextView) contactsAddedView.findViewById(R.id.tv_no_contacts);
            spinnerSort = (Spinner) contactsAddedView.findViewById(R.id.spinner_sort);
            tvSortType = (TextView) contactsAddedView.findViewById(R.id.sort_type);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                spinnerSort.setVisibility(View.VISIBLE);
                tvSortType.setVisibility(View.GONE);
                String[] myResArray = getResources().getStringArray(R.array.sortArray);
                List<String> mArray = Arrays.asList(myResArray);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_custom, mArray);
                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_custom);
                spinnerSort.setAdapter(spinnerArrayAdapter);
                spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @SuppressWarnings("ConstantConditions")
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            if (spinnerSort.getSelectedItemPosition() == 0) {
                                Collections.sort(contactList, new CompareID());
                                refreshList(contactList);
                            } else if (spinnerSort.getSelectedItemPosition() == 1) {
                                Collections.sort(contactList, new CompareDate());
                                refreshList(contactList);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            } else {
                spinnerSort.setVisibility(View.GONE);
                tvSortType.setVisibility(View.VISIBLE);
            }
            // Initialize AdMob View
            AdView adView = (AdView) contactsAddedView.findViewById(R.id.adView);
            AdRequest.Builder adBuilder = new AdRequest.Builder();
            adBuilder.addKeyword("Beer");
            adBuilder.addKeyword("Drunk");
            adBuilder.addKeyword("Alcohol");
            adBuilder.addKeyword("Food");
            adBuilder.addKeyword("Bar");
            adBuilder.addKeyword("Taxi");
            AdRequest adRequest = adBuilder.build();
            adView.loadAd(adRequest);
        }


        return contactsAddedView;
    }

    public void onRefreshStarted(View view) {
        try {
            Activity_Main.contactList = new ArrayList<Contact>();
            contactList = new ArrayList<Contact>();
            if (loadContactsList.getStatus() == AsyncTask.Status.RUNNING) {
                loadContactsList.cancel(true);
            }
            loadContactsList = new LoadContactsList();
            loadContactsList.execute();
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup viewGroup = (ViewGroup) view;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            // As we're using a ListFragment we create a PullToRefreshLayout manually
            mPullToRefreshLayout = new PullToRefreshLayout(viewGroup.getContext());

            // We can now setup the PullToRefreshLayout
            ActionBarPullToRefresh.from(getActivity())
                    // We need to insert the PullToRefreshLayout into the Fragment's ViewGroup
                    .insertLayoutInto(viewGroup)
                            // Here we mark just the ListView and it's Empty View as pullable
                    .theseChildrenArePullable(android.R.id.list, android.R.id.empty)
                    .listener(this)
                    .setup(mPullToRefreshLayout);
        }

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @SuppressWarnings("rawtypes")
            @Override
            public void onItemClick(AdapterView parent, View arg1, int position, long arg3) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                @SuppressWarnings("ConstantConditions") Uri uri = Uri.withAppendedPath(Contacts.CONTENT_URI, String.valueOf(contactList.get(position).getContactID()));
                intent.setData(uri);
                getActivity().startActivity(intent);
            }
        });
    }

    void refreshList(ArrayList<Contact> contacts) {
        try {
            if (contacts.size() > 0) {
                mListAdapter = new ContactListAdapter(getActivity(), R.layout.list_item_contact, contacts);
                setListAdapter(mListAdapter);
                mListAdapter.notifyDataSetChanged();
                llSort.setVisibility(View.VISIBLE);
            } else {
                llSort.setVisibility(View.GONE);
            }

        } catch (Exception ignored) {
            llSort.setVisibility(View.GONE);
        }

    }

    private Date getDate(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time));
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Activity_Main.contactList.size() > 0) {
                contactList = Activity_Main.contactList;
                try {
                    Collections.sort(contactList, new CompareID());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        spinnerSort.setSelection(0);
                    }
                } catch (Exception ignored) {
                }
                mListAdapter = new ContactListAdapter(getActivity(), R.layout.list_item_contact, contactList);
                setListAdapter(mListAdapter);
                mListAdapter.notifyDataSetChanged();
            } else {
                contactList = new ArrayList<Contact>();
                if (loadContactsList.getStatus() == AsyncTask.Status.RUNNING) {
                    loadContactsList.cancel(true);
                }
                loadContactsList = new LoadContactsList();
                loadContactsList.execute();
            }
        } catch (Exception ignored) {
            contactList = new ArrayList<Contact>();
            loadContactsList = new LoadContactsList();
            loadContactsList.execute();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (loadContactsList.getStatus() == AsyncTask.Status.RUNNING) {
                loadContactsList.cancel(true);
                if (llLoading.getVisibility() != View.GONE) {
                    llLoading.setVisibility(View.GONE);
                }
            }
            setListAdapter(null);
        } catch (Exception ignored) {
        }

    }

    private class LoadContactsList extends AsyncTask<String, String, String> {
        protected void onPreExecute() {
            //   if (refreshData && (!ptr)) {
            getListView().setVisibility(View.INVISIBLE);
            llLoading.setVisibility(View.VISIBLE);
            //    }
        }

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... params) {
            try {
                contactList = new ArrayList<Contact>();
                ContentResolver cr = getActivity().getContentResolver();
                @SuppressWarnings("ConstantConditions") Cursor cur = cr.query(Contacts.CONTENT_URI,
                        null, null, null, null);
                if (cur != null) {
                    if (cur.getCount() > 0) {
                        while (cur.moveToNext()) {
                            Contact newContact = new Contact();
                            String _id = cur.getString(
                                    cur.getColumnIndex(Contacts._ID));
                            String hasPhone = cur.getString(cur.getColumnIndex(Contacts.HAS_PHONE_NUMBER));

                            if (hasPhone != null) {
                                if (hasPhone.equalsIgnoreCase("1")) {
                                    Cursor phones = null;
                                    if (CONTENT_URI != null) {
                                        phones = getActivity().getContentResolver().query(CONTENT_URI, null, CONTACT_ID + " = " + _id,
                                                null, null
                                        );
                                    }
                                    if (phones != null) {

                                        newContact.setContactID(_id);
                                        newContact.setContactName(cur.getString(
                                                cur.getColumnIndex(Contacts.DISPLAY_NAME)));

                                        phones.moveToFirst();
                                        newContact.setContactPhone(phones.getString(phones.getColumnIndex("data1")));
                                        try {
                                            long timeStamp;
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {

                                                timeStamp = Long.valueOf(cur.getString(
                                                        cur.getColumnIndex(Contacts.CONTACT_LAST_UPDATED_TIMESTAMP)));
                                            } else {
                                                timeStamp = Long.valueOf(cur.getString(
                                                        cur.getColumnIndex(Contacts.CONTACT_STATUS_TIMESTAMP)));
                                            }
                                            newContact.setLastUpdated(timeStamp);
                                        } catch (Exception ignored) {
                                        }

                                        newContact.setContactPosition(contactList.size());
                                        contactList.add(newContact);
                                    }

                                }
                            }
                        }
                    }
                }
                Collections.reverse(contactList);
                return "success";
            } catch (Exception ignored) {
                return "error";
            }


        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Define Variables
            if (result.equals("success")) {
                try {
                    if (contactList.size() > 0) {
//                        test = mLoads.get(0);
                        llLoading.setVisibility(View.GONE);
                        tvNoResults.setVisibility(View.GONE);
                        Activity_Main.contactList = new ArrayList<Contact>();
                        Activity_Main.contactList = contactList;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            spinnerSort.setSelection(0);
                        }
                        mListAdapter = new ContactListAdapter(getActivity(), R.layout.list_item_contact, contactList);
                        setListAdapter(mListAdapter);
                        mListAdapter.notifyDataSetChanged();
                        //     Fragment_Menu_Load_ListNav.setNavigationBadge(currentNavigationItem, mLoads);
                        llSort.setVisibility(View.VISIBLE);
                        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
                            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                if (totalItemCount >= 2) {
                                    if (firstVisibleItem == 0) {
                                        if (llSort.getVisibility() == View.GONE) {
                                            Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
                                            llSort.setAnimation(anim);
                                            llSort.setVisibility(View.VISIBLE);
                                        }
                                        mLastFirstVisibleItem = firstVisibleItem;
                                    } else {
                                        if (firstVisibleItem > mLastFirstVisibleItem) {
                                            if (llSort.getVisibility() == View.VISIBLE) {
                                                Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
                                                llSort.setAnimation(anim);
                                                llSort.setVisibility(View.GONE);
                                            }
                                        } else if (firstVisibleItem < mLastFirstVisibleItem) {
                                            if (llSort.getVisibility() == View.GONE) {
                                                Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
                                                llSort.setAnimation(anim);
                                                llSort.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        mLastFirstVisibleItem = firstVisibleItem;
                                    }
                                }
                            }

                            public void onScrollStateChanged(AbsListView view, int scrollState) {
                                // TODO Auto-generated method stub
                            }
                        });
                    } else {
                        tvNoResults.setVisibility(View.VISIBLE);
                        llLoading.setVisibility(View.GONE);
                        llSort.setVisibility(View.GONE);
                        setListAdapter(null);
                        //   mListAdapter.notifyDataSetChanged();
                    }
                } catch (Exception ignored) {
                    tvNoResults.setVisibility(View.VISIBLE);
                    llLoading.setVisibility(View.GONE);
                    llSort.setVisibility(View.GONE);
                    setListAdapter(null);
                    //   mListAdapter.notifyDataSetChanged();
                }
                llLoading.setVisibility(View.GONE);

            }
            try {
                getListView().setVisibility(View.VISIBLE);
                //   ptr = false;
                //   mPullToRefreshLayout.setRefreshComplete();
            } catch (Exception ignored) {
            }
            try {
                mPullToRefreshLayout.setRefreshComplete();
            } catch (Exception ignored) {
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private class CompareDate implements Comparator<Contact> {
        @Override
        public int compare(Contact contact1, Contact contact2) {
            try {
                Date date1 = getDate(contact1.getLastUpdated());
                Date date2 = getDate(contact2.getLastUpdated());
                return date2.compareTo(date1);

            } catch (Exception e) {
                return String.valueOf(contact1.getLastUpdated()).compareTo(String.valueOf(contact2.getLastUpdated()));
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private class CompareID implements Comparator<Contact> {
        @Override
        public int compare(Contact contact1, Contact contact2) {
            //   SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
            try {
                //  Date date1 = getDate(contact1.getLastUpdated());
                //  Date date2 = getDate(contact2.getLastUpdated());
                return contact2.getContactID().compareTo(contact1.getContactID());

            } catch (Exception e) {
                return String.valueOf(contact1.getContactID()).compareTo(String.valueOf(contact2.getContactID()));
            }
        }
    }

    class ContactListAdapter extends ArrayAdapter<Contact> {

        private final ArrayList<Contact> contacts;

        private ContactListAdapter(Context context, int textViewResourceId, ArrayList<Contact> objects) {
            super(context, textViewResourceId, objects);
            this.contacts = objects;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_contact, null);
            }
            Contact mContact = contacts.get(position);
            // Initialize Variables
            if (convertView != null) {
                TextView tvName = (TextView) convertView.findViewById(R.id.tv_contact_name);
                TextView tvNumber = (TextView) convertView.findViewById(R.id.tv_contact_number);
                LinearLayout llRoot = (LinearLayout) convertView.findViewById(R.id.ll_list_item_contact);
                if (position == 0) {
                    float scale = getResources().getDisplayMetrics().density;
                    int dpAsPixels = (int) (42 * scale + 0.5f);
                    llRoot.setPadding(0, dpAsPixels, 0, 0);
                } else {
                    llRoot.setPadding(0, 0, 0, 0);
                }
                tvName.setText(mContact.getContactName());
                tvNumber.setText(mContact.getContactPhone());
            }

            return convertView;
        }

    }

}