package com.duesmobile.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.duesmobile.R;
import com.duesmobile.receivers.Receiver_Device_Admin;
import com.duesmobile.services.Service_Alarm_Two_AM;
import com.duesmobile.util.RepeatSafeToast;
import com.duesmobile.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

@SuppressWarnings("unused")
public class Fragment_Basic_Settings extends Fragment {
    public static int REQUEST_ENABLE = 17;
    View settingsView;
    String providerName;
    Intent mainService;
    RadioGroup mathDifficultyRadioGroup;
    RadioButton easyMath;
    RadioButton averageMath;
    RadioButton hardMath;
    RadioButton collegeMath;
    CheckBox cbAfterTwo;
    LinearLayout llDeviceAdmin;
    TextView tvDeviceAdminTitle;
    SeekBar seekDuration;
    TextView tvDurationText;
    DevicePolicyManager mDPM;
    ComponentName mAdminName;
    private LocationManager locationManager;
    private Location providerLocation;
    private Location currentGPS;
    private String TAG = "Fragment_Find_Freight_Main";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        settingsView = inflater.inflate(R.layout.fragment_basic_settings, container, false);
        setHasOptionsMenu(true);
        mDPM = (DevicePolicyManager) getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminName = new ComponentName(getActivity(), Receiver_Device_Admin.class);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final SharedPreferences.Editor prefEdit = prefs.edit();
        Util.CreateGAScreenView(getActivity(), "Basic Settings");

        mathDifficultyRadioGroup = (RadioGroup) settingsView.findViewById(R.id.rg_mathDifficulty);
        easyMath = (RadioButton) settingsView.findViewById(R.id.radio_easyMath);
        averageMath = (RadioButton) settingsView.findViewById(R.id.radio_averageMath);
        hardMath = (RadioButton) settingsView.findViewById(R.id.radio_hardMath);
        collegeMath = (RadioButton) settingsView.findViewById(R.id.radio_collegeMath);
        cbAfterTwo = (CheckBox) settingsView.findViewById(R.id.cb_afterTwo);
        llDeviceAdmin = (LinearLayout) settingsView.findViewById(R.id.ll_admin);
        tvDeviceAdminTitle = (TextView) settingsView.findViewById(R.id.tv_device_admin_title);
        seekDuration = (SeekBar) settingsView.findViewById(R.id.seek_duration);
        tvDurationText = (TextView) settingsView.findViewById(R.id.tv_duration_text);
        // Initialize AdMob View
        AdView adView = (AdView) settingsView.findViewById(R.id.adView);
        AdRequest.Builder adBuilder = new AdRequest.Builder();
        adBuilder.addKeyword("Beer");
        adBuilder.addKeyword("Drunk");
        adBuilder.addKeyword("Alcohol");
        adBuilder.addKeyword("Food");
        adBuilder.addKeyword("Bar");
        adBuilder.addKeyword("Taxi");
        AdRequest adRequest = adBuilder.build();
        adView.loadAd(adRequest);
        // Initialize views
        LoadPreferences();
        // Set Listeners
        llDeviceAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (mDPM.isAdminActive(mAdminName)) {
                    if (!Util.isMyServiceRunning(getActivity())) {
                        mDPM.removeActiveAdmin(mAdminName);
                        tvDeviceAdminTitle.setText(R.string.txt_enable_admin);
                    } else {
                        tvDeviceAdminTitle.setText(R.string.txt_disable_admin);
                        Toast.makeText(getActivity(), "You must unlock Drunk Lock in order to change Admin privileges.", Toast.LENGTH_LONG).show();
                    }
                } else if (!mDPM.isAdminActive(mAdminName)) {
                    tvDeviceAdminTitle.setText(R.string.txt_enable_admin);
                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                            mAdminName);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                            "Drunk Lock is asking to be an active device administrator to prevent it from being force " +
                                    "stopped while active. When activated, you will be easily able to revoke access by going to basic settings."
                    );
                    startActivityForResult(intent, REQUEST_ENABLE);
                }
            }
        });
        seekDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!Util.isMyServiceRunning(getActivity())) {
                    prefEdit.putInt(Util.KEY_DURATION_PROGRESS, seekDuration.getProgress()).commit();
                    setDurationText(progress);
                } else {
                    LoadPreferences();
                    RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        mathDifficultyRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (!Util.isMyServiceRunning(getActivity())) {
                    if (checkedId == easyMath.getId()) {
                        prefEdit.putString(Util.KEY_MATH_DIFFICULTY, "easy").commit();
                    } else if (checkedId == averageMath.getId()) {
                        prefEdit.putString(Util.KEY_MATH_DIFFICULTY, "average").commit();
                    } else if (checkedId == hardMath.getId()) {
                        prefEdit.putString(Util.KEY_MATH_DIFFICULTY, "hard").commit();
                    } else if (checkedId == collegeMath.getId()) {
                        prefEdit.putString(Util.KEY_MATH_DIFFICULTY, "college").commit();
                    }
                } else {
                    LoadPreferences();
                    RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                }
            }
        });
        cbAfterTwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (!Util.isMyServiceRunning(getActivity())) {
                    if (buttonView.isChecked()) {
                        prefEdit.putBoolean(Util.KEY_ENABLE_AFTER_TWO, true).commit();
                    } else {
                        prefEdit.putBoolean(Util.KEY_ENABLE_AFTER_TWO, false).commit();
                    }
                } else {
                    RepeatSafeToast.show(getActivity(), "You must disable drunk lock in order to change this setting.");
                    buttonView.setChecked(!isChecked);
                }
            }
        });

        return settingsView;
    }


    public void LoadPreferences() {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            // Math Difficulty
            if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("average")) {
                averageMath.setChecked(true);
            } else if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("hard")) {
                hardMath.setChecked(true);
            } else if (prefs.getString(Util.KEY_MATH_DIFFICULTY, "").equals("college")) {
                collegeMath.setChecked(true);
            } else {
                easyMath.setChecked(true);
            }
            // After Two
            if (prefs.getBoolean(Util.KEY_ENABLE_AFTER_TWO, false)) {
                cbAfterTwo.setChecked(true);
            } else {
                cbAfterTwo.setChecked(false);
            }
            // Device Admin
            if (mDPM.isAdminActive(mAdminName)) {
                tvDeviceAdminTitle.setText(R.string.txt_disable_admin);
            } else {
                tvDeviceAdminTitle.setText(R.string.txt_enable_admin);
            }
            // Duration
            int duration = prefs.getInt(Util.KEY_DURATION_PROGRESS, 0);
            seekDuration.setProgress(duration);
            setDurationText(duration);
        } catch (Exception ignored) {

        }
    }

    /* Called when the second activity's finished */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (REQUEST_ENABLE == requestCode) {
            if (resultCode == Activity.RESULT_OK) {
                tvDeviceAdminTitle.setText(R.string.txt_disable_admin);
            } else {
                tvDeviceAdminTitle.setText(R.string.txt_enable_admin);
            }
        }
    }


    private void setDurationText(int progress) {
        String suffix = " hrs";
        String time = "Until Unlocked";
        boolean isZero;
        double hourProgress = ((double) progress / 2);
        if (hourProgress <= ((double) 1)) {
            suffix = " hr";
        }
        if (hourProgress > ((double) 0)) {
            isZero = false;
            time = String.valueOf(hourProgress).replace(".0", "");
        } else {
            isZero = true;
        }
        if (isZero) {
            tvDurationText.setText(time);
        } else {
            tvDurationText.setText(time + suffix);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LoadPreferences();
        if (mDPM.isAdminActive(mAdminName)) {
            tvDeviceAdminTitle.setText(R.string.txt_disable_admin);
        } else {
            tvDeviceAdminTitle.setText(R.string.txt_enable_admin);
        }
    }

    @Override
    public void onPause() {
        if (!Util.isMyServiceRunning(getActivity())) {
            Intent AlarmTwoAMService = new Intent(getActivity(), Service_Alarm_Two_AM.class);
            getActivity().startService(AlarmTwoAMService);
        }

    }

}