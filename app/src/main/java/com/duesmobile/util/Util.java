package com.duesmobile.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.duesmobile.R;
import com.duesmobile.services.Service_Drunk_Lock;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

public class Util {

    public static final String PREFS_NAV_START = "firstStart";
    public static final String KEY_ON_STARTUP = "startOnBootup";
    public static final String KEY_CONTACT_BLOCK_ALL = "contact_block_all";
    private static final String DRUNK_LOCK_PRO_URL = "http://tinyurl.com/drunklockpro";
    public static final String KEY_MATH_DIFFICULTY = "math_difficulty";
    public static final String KEY_ENABLE_AFTER_TWO = "after_two";
    public static final String KEY_BLOCK_CALLS = "block_calls";
    public static final String KEY_BLOCK_SMS = "block_s";
    public static final String KEY_START_ON_STARTUP = "startup";
    public static final String KEY_DURATION_PROGRESS = "duration_progress";
    private static final String DRUNK_LOCK_URL = "https://play.google.com/store/apps/details?id=com.duesmobile.drunklock";
    private static final String drunkLockProName = "com.duesmobile.drunklockpro";
    private static final String GA_DRUNK_LOCK_CATEGORY = "drunk_lock";
    private static final String GA_DRUNK_LOCK_PRO_CATEGORY = "drunk_lock_pro";
    private final static String GA_DRUNK_LOCK_PRO_UPDATE = "drunk_lock_pro_ad";
    public static Integer questionCount = 1;
    public static Integer numTimes = 3;
    public static boolean activityMainStarted = false;
    public static Activity activity = null;
    public static String blockType = "Drunk Lock is blocking all outgoing calls and text messages.";
    private static PendingIntent durationPendingIntent;
    public static Tracker mTracker = null;
    public static synchronized Tracker getTracker(Context mContext) {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(mContext);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }

        return mTracker;
    }
    public static void CreateGAScreenView(Context mContext, String mScreenName) {
        Tracker t = getTracker(mContext);
        t.setScreenName(mScreenName);
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public static void CreateGAEvent(Context mContext, String mActionName, String mLabelID) {
        Tracker t = getTracker(mContext);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(GA_DRUNK_LOCK_CATEGORY)
                .setAction(mActionName)
                .setLabel(mLabelID)
                .build());
    }

    public static void OpenDrunkLockPro(Context mContext, String logString) {
        CreateGAEvent(mContext, GA_DRUNK_LOCK_PRO_UPDATE, logString);
        try {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + drunkLockProName)));
        } catch (Exception ignored) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + drunkLockProName)));
        }
    }


    public static boolean isMyServiceRunning(Context cxt) {
        ActivityManager manager = (ActivityManager) cxt.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (Service_Drunk_Lock.class.getName().equals(service.service.getClassName())) {
                Log.e("Running", "True");
                return true;
            }
        }
        Log.e("Running", "False");
        return false;
    }

    public static boolean hasTelephony(Context mContext) {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getPhoneType() != TelephonyManager.PHONE_TYPE_NONE;

    }

    public static Button CreateShareButton(final Context cxt, final String shareText) {
        Button btn = new Button(cxt);
        btn.setText("Share");
        btn.setTextColor(Color.WHITE);
        btn.setBackgroundResource(R.drawable.btn_drunk_lock);
        btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_share, 0, 0, 0);
        btn.setPadding(10, 5, 50, 5);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 10, 10, 10);
        btn.setLayoutParams(params);
        // btn.setlayo
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = shareText + " #DrunkLock " + Util.DRUNK_LOCK_PRO_URL;
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);
                cxt.startActivity(Intent.createChooser(share, "Share"));
            }
        });
        return btn;
    }


    private static class MultiChoiceAdapter extends ArrayAdapter<String> {

        public MultiChoiceAdapter(Context context, int resID, ArrayList<String> items) {
            super(context, resID, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            if (v != null) {
                ((TextView) v).setTextColor(Color.WHITE);
            }

            return v;
        }

    }


}
