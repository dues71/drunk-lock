package com.duesmobile.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DrunkLockDB {
    @SuppressWarnings("unused")
    private static final String TAG = "DrunkLockDB";

    // Declaration of Applications Table Variables
    private static final String APPLICATIONS_TABLE = "ApplicationsTable";
    private static final String APPLICATION_ID = "a_id";
    private static final String APPLICATION_NAME = "a_name";
    private static final String APPLICATION_PACKAGE_NAME = "a_package_name";
    private static final String DATABASE_CREATE_APPLICATIONS_TABLE = "create table " + APPLICATIONS_TABLE + " (" + APPLICATION_ID + " integer primary key autoincrement, " + APPLICATION_NAME
            + " text not null, " + APPLICATION_PACKAGE_NAME + " text not null);";
    // Declaration of Contacts Table Variables
    private static final String CONTACTS_TABLE = "ContactsTable";
    private static final String CONTACT_ID = "c_id";
    private static final String CONTACT_NAME = "c_name";
    private static final String CONTACT_NUMBER = "c_number";
    private static final String DATABASE_CREATE_CONTACTS_TABLE = "create table " + CONTACTS_TABLE + " (" + CONTACT_ID + " integer primary key autoincrement, " + CONTACT_NAME + " text not null, "
            + CONTACT_NUMBER + " text not null);";
    // Declaration of Alarm Table Variables
    private static final String ALARMS_TABLE = "AlarmsTable";
    private static final String ALARM_ID = "a_id";
    private static final String ALARM_START_HOUR = "a_start_hour";
    private static final String ALARM_START_MINUTE = "a_start_minute";
    private static final String ALARM_LENGTH = "a_length";
    private static final String ALARM_REPEATING = "a_repeating";
    private static final String ALARM_DAYS_REPEATING = "a_days_repeating";
    private static final String ALARM_ALWAYS_ON = "a_a_always_on";
    private static final String ALARM_STATUS = "a_status";
    private static final String DATABASE_CREATE_ALARMS_TABLE = "create table " + ALARMS_TABLE + " (" + ALARM_ID + " integer primary key autoincrement, " + ALARM_START_HOUR + " integer not null, "
            + ALARM_START_MINUTE + " integer not null, " + ALARM_LENGTH + " text not null, " + ALARM_REPEATING + " boolean not null, " + ALARM_DAYS_REPEATING + " text not null, " + ALARM_ALWAYS_ON + " boolean not null, " + ALARM_STATUS
            + " boolean not null);";
    private static final String DATABASE_NAME = "DrunkLockDB";
    private static final int DATABASE_VERSION = 1;
    private final DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DrunkLockDB(Context ctx) {
        Context context = ctx;
        DBHelper = new DatabaseHelper(context);

    }

    // ---opens the database---
    public DrunkLockDB open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    // ---closes the database---
    public void close() {
        DBHelper.close();
    }

    public Cursor query(String query) {
        db.execSQL(query);
        return null;
    }

    // ---insert an Application into the database---
    public long insertApplication(String a_name, String a_package_name) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(APPLICATION_NAME, a_name);
        initialValues.put(APPLICATION_PACKAGE_NAME, a_package_name);
        return db.insert(APPLICATIONS_TABLE, null, initialValues);
    }

    // ---insert a Contact into the database---
    public long insertContact(String c_name, String c_number) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(CONTACT_NAME, c_name);
        initialValues.put(CONTACT_NUMBER, c_number);
        return db.insert(CONTACTS_TABLE, null, initialValues);
    }

    // ---insert an Alarm into the database---
    public long insertAlarm(Integer startHour, Integer startMinute, String a_length, boolean a_repeating, String a_days_repeating, boolean a_always_on, boolean a_status) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(ALARM_START_HOUR, startHour);
        initialValues.put(ALARM_START_MINUTE, startMinute);
        initialValues.put(ALARM_LENGTH, a_length);
        initialValues.put(ALARM_REPEATING, a_repeating);
        initialValues.put(ALARM_DAYS_REPEATING, a_days_repeating);
        initialValues.put(ALARM_ALWAYS_ON, a_always_on);
        initialValues.put(ALARM_STATUS, a_status);
        return db.insert(ALARMS_TABLE, null, initialValues);
    }

    // ---deletes an Application---
    public boolean deleteApplication(long a_id) {

        return db.delete(APPLICATIONS_TABLE, APPLICATION_ID + "=" + a_id, null) > 0;
    }

    // ---deletes a Contact---
    public boolean deleteContact(long c_id) {

        return db.delete(CONTACTS_TABLE, CONTACT_ID + "=" + c_id, null) > 0;
    }

    // ---deletes an Alarm---
    public boolean deleteAlarm(long a_id) {

        return db.delete(ALARMS_TABLE, ALARM_ID + "=" + a_id, null) > 0;
    }

    // ---deletes all records of the Applications Table
    public void deleteAllApplications() {
        this.db.delete(APPLICATIONS_TABLE, null, null);
    }

    // ---deletes all records of the Contacts Table
    public void deleteAllContacts() {
        this.db.delete(CONTACTS_TABLE, null, null);
    }

    // ---deletes all records of the Alarms Table
    public void deleteAllAlarms() {
        this.db.delete(ALARMS_TABLE, null, null);
    }

    // ---retrieves all the Applications---
    public Cursor getAllApplications() {
        return db.query(APPLICATIONS_TABLE, new String[]{APPLICATION_ID, APPLICATION_NAME, APPLICATION_PACKAGE_NAME}, null, null, null, null, null, null);

    }

    // ---retrieves all the Contacts---
    public Cursor getAllContacts() {
        return db.query(CONTACTS_TABLE, new String[]{CONTACT_ID, CONTACT_NAME, CONTACT_NUMBER}, null, null, null, null, null, null);

    }

    // ---retrieves all the Alarms---
    public Cursor getAllAlarms() {
        return db.query(ALARMS_TABLE, new String[]{ALARM_ID, ALARM_START_HOUR, ALARM_START_MINUTE, ALARM_LENGTH, ALARM_REPEATING, ALARM_DAYS_REPEATING, ALARM_ALWAYS_ON, ALARM_STATUS}, null, null, null, null, null,
                null);

    }

    // ---retrieves a particular Application---
    public Cursor getApplication(long a_id) throws SQLException {
        Cursor mCursor = db.query(true, APPLICATIONS_TABLE, new String[]{APPLICATION_ID, APPLICATION_NAME, APPLICATION_PACKAGE_NAME}, APPLICATION_ID + "=" + a_id, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    // ---retrieves a particular Contact---
    public Cursor getContact(long c_id) throws SQLException {
        Cursor mCursor = db.query(true, CONTACTS_TABLE, new String[]{CONTACT_ID, CONTACT_NAME, CONTACT_NUMBER}, CONTACT_ID + "=" + c_id, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    // ---retrieves a particular Alarm---
    public Cursor getAlarm(long a_id) throws SQLException {
        Cursor mCursor = db.query(true, ALARMS_TABLE, new String[]{ALARM_ID, ALARM_START_HOUR, ALARM_START_MINUTE, ALARM_LENGTH, ALARM_REPEATING, ALARM_DAYS_REPEATING, ALARM_ALWAYS_ON, ALARM_STATUS}, ALARM_ID + "="
                + a_id, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    // ---updates an Application---
    public boolean updateApplication(long a_id, String a_name, String a_package_name) {
        ContentValues args = new ContentValues();
        args.put(APPLICATION_NAME, a_name);
        args.put(APPLICATION_PACKAGE_NAME, a_package_name);

        return db.update(APPLICATIONS_TABLE, args, APPLICATION_ID + "=" + a_id, null) > 0;
    }

    // ---updates a Contact---
    public boolean updateContact(long c_id, String c_name, String c_number) {
        ContentValues args = new ContentValues();
        args.put(CONTACT_NAME, c_name);
        args.put(CONTACT_NUMBER, c_number);

        return db.update(CONTACTS_TABLE, args, CONTACT_ID + "=" + c_id, null) > 0;
    }

    // ---updates an Alarm---
    public boolean updateAlarm(long a_id, Integer startHour, Integer startMinute, String a_length, boolean a_repeating, String a_days_repeating, boolean a_always_on, boolean a_status) {
        ContentValues args = new ContentValues();
        args.put(ALARM_START_HOUR, startHour);
        args.put(ALARM_START_MINUTE, startMinute);
        args.put(ALARM_LENGTH, a_length);
        args.put(ALARM_REPEATING, a_repeating);
        args.put(ALARM_DAYS_REPEATING, a_days_repeating);
        args.put(ALARM_ALWAYS_ON, a_always_on);
        args.put(ALARM_STATUS, a_status);
        return db.update(ALARMS_TABLE, args, ALARM_ID + "=" + a_id, null) > 0;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE_APPLICATIONS_TABLE);
            db.execSQL(DATABASE_CREATE_CONTACTS_TABLE);
            db.execSQL(DATABASE_CREATE_ALARMS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // AUTO Log.w(TAG, "Upgrading database from version " + oldVersion +
            // " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + APPLICATIONS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + CONTACTS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + ALARMS_TABLE);
            onCreate(db);
        }
    }

}